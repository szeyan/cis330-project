QT       += core gui network multimedia widgets
TEMPLATE = app
TARGET = Frameless
INCLUDEPATH += ./../src

SOURCES += \
    main.cpp \
    ./../src/NcFramelessHelper.cpp \
    Dialog.cpp

HEADERS += \
    ./../src/NcFramelessHelper.h \
    Dialog.h
