#-------------------------------------------------
#
# Project created by QtCreator 2014-02-16T20:34:12
#
#-------------------------------------------------

QT       += core gui network multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OinkChat
TEMPLATE = app


SOURCES += main.cpp\
    login.cpp \
    about.cpp \
    register.cpp \
    accinfo.cpp \
    forgot.cpp \
    chatroom.cpp \
    Cipher/date.cpp \
    oinkconfigurator.cpp \
    NcFramelessHelper/src/NcFramelessHelper.cpp \
    Protocol/messageobject.cpp \
    Protocol/oinkobject.cpp \
    Protocol/userobject.cpp \
    SMTP/smtp.cpp

HEADERS  += \
    about.hpp \
    forgot.hpp \
    accinfo.hpp \
    login.hpp \
    register.hpp \
    chatroom.hpp \
    Cipher/date.hpp \
    oinkconfigurator.hpp \
    NcFramelessHelper/src/NcFramelessHelper.h \
    Protocol/messageobject.hpp \
    Protocol/oinkobject.hpp \
    Protocol/userobject.hpp \
    SMTP/smtp.h

FORMS    += \
    login.ui \
    about.ui \
    register.ui \
    accinfo.ui \
    forgot.ui \
    chatroom.ui

RESOURCES += \
    resource.qrc

OTHER_FILES += \
    resource/stylesheet.css

win32:RC_FILE += resource/windowsicon.rc

DEFINES += PROJECT_PATH=\"\\\"$$PWD\\\"\"
DEFINES += BUILD_PATH=\"\\\"$$OUT_PWD\\\"\"
