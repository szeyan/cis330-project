#include "oinkobject.hpp"

OinkObject::OinkObject() : ACK(false), flag("")
{

}

OinkObject::OinkObject(bool ACK, UserObject user, MessageObject message, QVector<QString> userList, QString flag):
    ACK(ACK), user(user), message(message), userList(userList), flag(flag) {}


OinkObject::~OinkObject()
{

}

bool OinkObject::isAck() const
{
    return ACK;
}

void OinkObject::setAck(bool ack)
{
    ACK = ack;
}

const MessageObject &OinkObject::getMessage() const
{
    return message;
}

void OinkObject::setMessage(const MessageObject &message)
{
    this->message = message;
}

UserObject OinkObject::getUser() const
{
    return user;
}

void OinkObject::setUser(UserObject user)
{
    this->user = user;
}

const QVector<QString> &OinkObject::getUserList() const
{
    return userList;
}

void OinkObject::setUserList(const QVector<QString> &userList)
{
    this->userList = userList;
}

QString OinkObject::getFlag() const
{
    return flag;
}

void OinkObject::setFlag(QString flag)
{
    this->flag = flag;
}

QDataStream &operator<<(QDataStream &out, const OinkObject &oinkObject)
{
    out << oinkObject.isAck()
        << oinkObject.getMessage() << oinkObject.getUser()
        << oinkObject.getFlag()
        << oinkObject.getUserList();
    return out;
}

QDataStream &operator>>(QDataStream &in, OinkObject &oinkObject)
{
    bool ACK;
    UserObject user;
    MessageObject message;
    QVector<QString> userList;
    QString flag;

    in >> ACK
       >> message >> user >> flag >> userList;

    oinkObject = OinkObject(ACK, user, message, userList, flag);
    return in;
}
