#ifndef OINKOBJECT_HPP
#define OINKOBJECT_HPP
#include <QVector>
#include <QString>
#include <QDataStream>
#include "messageobject.hpp"
#include "userobject.hpp"

/**
 * OinkObjects are transferred between the server and client.
 * Within the object contains a flag string, which is read by both client/server to determine how it should handle the OinkObject
 * So, essentially this class encapsulates our Oink Communication Protocol
 *
 * @author Hanxiao Zhang, Xingwei Cao
 */
class OinkObject
{

    public:
        /**
         * Constructor: we need to explicitly define the default constructor for Qt serialization
         */
        OinkObject();

        /**
         * Constructor:
         * @param ACK whether the message was acknowledged (valid/invalid)
         * @param user is an object, which holds the username, password, and email of the user
         * @param message is an object, which holds the "message" sent over the chat, the sender, and the receiver
         * @param userList is a list, which contains the list of active users connected to OinkChat
         * @param flag determines how the server/client should handle the OinkObject
         */
        OinkObject(bool ACK, UserObject user, MessageObject message, QVector<QString> userList, QString flag);

        /**
         * Destructor
         */
        ~OinkObject();

        /**
         * @return the acknowledgement
         */
        bool isAck() const;

        /**
         * Set the acknowledgement
         * @param ack the acknowledgement
         */
        void setAck(bool ack);

        /**
         * @return the message object, which holds the "message" sent over the chat, the sender, and the receiver
         */
        const MessageObject &getMessage() const;

        /**
         * Set the message object
         * @param message the message object
         */
        void setMessage(const MessageObject &message);

        /**
         * @return the user object, which holds the username, password, and email of the user
         */
        UserObject getUser() const;

        /**
         * Set the user object
         * @param user the user object
         */
        void setUser(UserObject user);

        /**
         * @return a list, which contains the list of active users connected to OinkChat
         */
        const QVector<QString> &getUserList() const;

        /**
         * Set the list of active users connected to OinkChat
         * @param userList the list of active users
         */
        void setUserList(const QVector<QString> &userList);

        /**
         * @return the flag, which determines how the server/client should handle the OinkObject
         */
        QString getFlag() const;

        /**
         * Sets the flag, which determines how the server/client should handle the OinkObject
         * @param flag the name of the request
         */
        void setFlag(QString flag);

        /**
         * Overloading operator: out
         */
        friend QDataStream &operator<<(QDataStream &out, const OinkObject &oinkObject);

        /**
         * Overloading operator: in
         */
        friend QDataStream &operator>>(QDataStream &in, OinkObject &oinkObject);

    private:
        bool ACK; ///< whether the request was acknowledged
        UserObject user; ///< holds the username, password, and email of the user
        MessageObject message; ///< holds the "message" sent over the chat, the sender, and the receiver
        QVector<QString> userList; ///< contains the list of active users connected to OinkChat
        QString flag; ///< flags can be "register", "login", "update", "send", "forgotAcc";
};

#endif
