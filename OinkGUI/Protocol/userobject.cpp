#include <QDataStream>
#include "userobject.hpp"

UserObject::UserObject(): name(""), password(""), email("") {}

UserObject::UserObject(QString name, QString password, QString email):
    name(name), password(password), email(email) {}

UserObject::~UserObject() {}

void UserObject::setName(QString _name)
{
    this->name = _name;
}

QString UserObject::getName() const
{
    return this->name;
}

void UserObject::setPassword(QString _password)
{
    this->password = _password;
}

QString UserObject::getPassword() const
{
    return this->password;
}

void UserObject::setEmail(QString _email)
{
    this->email = _email;
}

QString UserObject::getEmail() const
{
    return this->email;
}

QDataStream &operator<<(QDataStream &out, const UserObject &userObject)
{
    out << userObject.getName() << userObject.getPassword()
        << userObject.getEmail();
    return out;
}

QDataStream &operator>>(QDataStream &in, UserObject &userObject)
{
    QString name;
    QString password;
    QString email;
    in >> name >> password >> email;
    userObject = UserObject(name, password, email);
    return in;
}
