#ifndef USEROBJECT_HPP
#define USEROBJECT_HPP

#include <QString>
#include <QDataStream>

/**
 * User object holds the username, password, and email of a user account
 *
 * @author Xingwei Cao, Hanxiao Zhang
 */
class UserObject
{

    public:
        /**
         * Constructor: we need to explicitly define the default constructor for Qt serialization
         */
        UserObject();

        /**
         * Constructor
         * @param name the username of this user
         * @param password of this user
         * @param email of this user
         */
        UserObject(QString name, QString password, QString email);

        /**
         * Destructor
         */
        ~UserObject();

        /**
         * Sets the username of this user
         * @param _name the name of this user
         */
        void setName(QString _name);

        /**
         * @return the username of this user
         */
        QString getName() const;

        /**
         * Sets the password of this user
         * @param _password the password of this user
         */
        void setPassword(QString _password);

        /**
         * @return the password of this user
         */
        QString getPassword() const;

        /**
         * Sets the email of this user
         * @param _email the email of this user
         */
        void setEmail(QString _email);

        /**
         * @return the email of this user
         */
        QString getEmail() const;

        /**
         * Overloading operator: out
         */
        friend QDataStream &operator<<(QDataStream &out, const UserObject &userObject);

        /**
         * Overloading operator: in
         */
        friend QDataStream &operator>>(QDataStream &in, UserObject &userObject);

    private:
        QString name; ///< username of this user
        QString password; ///< password of this user
        QString email; ///< email of this user

};

#endif //USEROBJECT_HPP
