#include <QtCore>
#include <QtGui>
#include <QFile>
#include <QFont>
#include "about.hpp"
#include "ui_about.h"

About::About(QWidget *parent) :
    QDialog(parent), ui(new Ui::About),
    minmaxSwitch(true), framelessHelper(NULL), effect(NULL)
{
    //--we want no frames and we want our own custom shadows
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    flags |= Qt::NoDropShadowWindowHint;
    this->setWindowFlags(flags);

    //--add a context menu
    QAction *quitAction = new QAction(tr("E&xit"), this);
    quitAction->setObjectName("rightcontextmenu");
    quitAction->setShortcut(tr("Ctrl+Q"));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    addAction(quitAction);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    //--set up the ui (must be set up before setting the font for widgets)
    ui->setupUi(this);
    this->setWindowTitle("");

    //--set style sheet
    this->setStyleSheet(oinker.getStyleSheet());

    //--add a dropshadow to our form
    this->setAttribute(Qt::WA_TranslucentBackground); //hide main window
    effect = new QGraphicsDropShadowEffect();
    effect->setBlurRadius(15);
    effect->setColor(QColor("#000000"));
    effect->setOffset(QPoint(effect->xOffset() - 2, effect->yOffset() - 2));
    ui->shadower->setGraphicsEffect(effect);
    ui->shadower->setStyleSheet("#shadower { background-color: #6F2E50; color: #F5F5F5;}");

    //--set up how we want our frameless window
    framelessHelper = new NcFramelessHelper;
    framelessHelper->activateOn(this);
    framelessHelper->setWidgetMovable(true);
    framelessHelper->setWidgetResizable(true);

    //--set the fonts for the labels/widgets
    oinker.loadFonts();
    ui->about_title->setFont(oinker.getUbuntuLight());
    ui->about_text->setFont(oinker.getUbuntuRegular());

}

About::~About()
{
    delete effect;
    delete framelessHelper;
    delete ui;
}

void About::on_quitButton_clicked()
{
    this->close();
}
