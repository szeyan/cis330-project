#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>
#include <QGraphicsDropShadowEffect>
#include "NcFramelessHelper/src/NcFramelessHelper.h"
#include "oinkconfigurator.hpp"

namespace Ui
{
class About;
}
/**
 * The about GUI is responsible for showing some of the credits
 * It'll basically pop up a little blurb about the Flying Piglets Team
 *
 * @author SzeYan Li
 */
class About : public QDialog
{
        Q_OBJECT

    public:
        /**
         * Constructor
         */
        explicit About(QWidget *parent = 0);

        /**
         * Destructor
         */
        ~About();

    private slots:
        /**
         * Simulates the quit button
         */
        void on_quitButton_clicked();

    private:
        Ui::About *ui; ///< The gui connected to this class
        OinkConfigurator oinker; ///< a configurator for fonts/styles/encrypt/decrypt
        bool minmaxSwitch; ///< used by on_minmaxButton_clicked() to know when to minimize or maximize
        NcFramelessHelper *framelessHelper; ///< used to create frameless gui window
        QGraphicsDropShadowEffect *effect; ///< used to create a dropshadow effect for the gui
};

#endif // ABOUT_H
