#include "accinfo.hpp"
#include "ui_accinfo.h"

AccInfo::AccInfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AccInfo),
    framelessHelper(NULL), effect(NULL)
{

    //--we want no frames and we want our own custom shadows
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    flags |= Qt::NoDropShadowWindowHint;
    this->setWindowFlags(flags);

    //--add a context menu
    QAction *quitAction = new QAction(tr("E&xit"), this);
    quitAction->setObjectName("rightcontextmenu");
    quitAction->setShortcut(tr("Ctrl+Q"));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    addAction(quitAction);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    //--set up the ui (must be set up before setting the font for widgets)
    ui->setupUi(this);
    this->setWindowTitle("");

    //--set style sheet
    this->setStyleSheet(oinker.getStyleSheet());

    //--set the fonts for the labels/widgets (because macs can't load custom fonts, we'll just load images
#if defined(Q_OS_WIN) || defined(Q_OS_UNIX) //tested to be working on windows and unix at least
    oinker.loadFonts();
    ui->username_label->setFont(oinker.getUbuntuCondensed());
    ui->pwd_label->setFont(oinker.getUbuntuCondensed());
    ui->username_info->setFont(oinker.getUbuntuCondensed());
    ui->pwd_info->setFont(oinker.getUbuntuCondensed());
#endif

#if defined(Q_OS_MAC) || (!defined(Q_OS_WIN) && !defined(Q_OS_UNIX))
    ui->username_label->setText("");
    ui->username_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/username.png) no-repeat left center;");
    ui->pwd_label->setText("");
    ui->pwd_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/password.png) no-repeat left center;");
#endif

    //--add a dropshadow to our form
    this->setAttribute(Qt::WA_TranslucentBackground); //hide main window
    effect = new QGraphicsDropShadowEffect();
    effect->setBlurRadius(15);
    effect->setColor(QColor("#000000"));
    effect->setOffset(QPoint(effect->xOffset() - 2, effect->yOffset() - 2));
    ui->shadower->setGraphicsEffect(effect);
    ui->shadower->setStyleSheet("#shadower { background-color: #6F2E50; color: #F5F5F5;}");

    //--set up how we want our frameless window
    framelessHelper = new NcFramelessHelper;
    framelessHelper->activateOn(this);
    framelessHelper->setWidgetMovable(true);
    framelessHelper->setWidgetResizable(true);
}

AccInfo::~AccInfo()
{
    delete effect;
    delete framelessHelper;
    delete ui;
}

void AccInfo::setUsername(QString name)
{
    ui->username_info->setText(name);
}

void AccInfo::setPassword(QString password)
{
    ui->pwd_info->setText(password);
}

void AccInfo::on_quitButton_clicked()
{
    this->close();
}
