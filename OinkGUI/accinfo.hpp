#ifndef ACCINFO_H
#define ACCINFO_H

#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsDropShadowEffect>
#include "NcFramelessHelper/src/NcFramelessHelper.h"
#include "oinkconfigurator.hpp"

namespace Ui
{
class AccInfo;
}

/**
 * The account info gui is responsible for showing the username and password
 * after the forgot username/password form is filled out
 *
 * @author SzeYan Li
 */
class AccInfo : public QDialog
{
        Q_OBJECT

    public:
        /**
         * Constructor
         */
        explicit AccInfo(QWidget *parent = 0);
        /**
         * Destructor
         */
        ~AccInfo();

        /**
         * Sets the name of this user
         * @param name username of this user
         */
        void setUsername(QString name);

        /**
         * Sets the name of this user
         * @param password password of this user
         */
        void setPassword(QString password);

    private slots:
        /**
         * Simulates the quit button
         */
        void on_quitButton_clicked();

    private:
        //GUI elements
        Ui::AccInfo *ui; ///< The gui connected to this class
        OinkConfigurator oinker; ///< a configurator for fonts/styles/encrypt/decrypt
        bool minmaxSwitch; ///< used by on_minmaxButton_clicked() to know when to minimize or maximize
        NcFramelessHelper *framelessHelper; ///< used to create frameless gui window
        QGraphicsDropShadowEffect *effect; ///< used to create a dropshadow effect for the gui
};

#endif // ACCINFO_H
