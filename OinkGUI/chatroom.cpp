#include <QtNetwork>
#include <QTimer>
#include <QTcpServer>
#include <QTcpSocket>
#include <QSound>
#include <QTextDocument>
#include "Protocol/oinkobject.hpp"
#include "chatroom.hpp"
#include "ui_chatroom.h"

Chatroom::Chatroom(QWidget *parent) : QDialog(parent),
    targetIpAddress("127.0.0.1"), targetPort(252525),
    ui(new Ui::Chatroom),
    minmaxSwitch(true), framelessHelper(NULL), effect(NULL),
    defaultChat("[Chatroom]"), currentChat(defaultChat), username(""),
    soundSwitch(true), fontMenuSwitch(true)
{
    //--init the gui and other elements
    initGUI();

    //--set up the socket
    tcpSocket = new QTcpSocket(this);
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readMessage()));
}

void Chatroom::initGUI()
{
    //--we want no frames and we want our own custom shadows
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    flags |= Qt::NoDropShadowWindowHint;
    this->setWindowFlags(flags);

    //--add a context menu
    QAction *quitAction = new QAction(tr("E&xit"), this);
    quitAction->setObjectName("rightcontextmenu");
    quitAction->setShortcut(tr("Ctrl+Q"));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    addAction(quitAction);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    //--set up the ui (must be set up before setting the font for widgets)
    ui->setupUi(this);
    this->setWindowTitle("");

    //--set style sheet
    this->setStyleSheet(oinker.getStyleSheet());

    //--set the fonts for the labels/widgets (because macs can't load custom fonts, we'll just load images
#if defined(Q_OS_WIN) || defined(Q_OS_UNIX) //tested to be working on windows and unix at least
    oinker.loadFonts();
    ui->enter_button->setFont(oinker.getUbuntuCondensed());
    ui->font_label->setFont(oinker.getUbuntuCondensed());
    ui->color_label->setFont(oinker.getUbuntuCondensed());
#endif

#if defined(Q_OS_MAC) || (!defined(Q_OS_WIN) && !defined(Q_OS_UNIX))
    ui->enter_button->setText("");
    ui->enter_button->setStyleSheet("background: #FDB71F url(:/src/resource/macsupport/enter.png) no-repeat center;");
#endif

//bug report: showMinimized and showMaximized doesn't work on Macs. It's a Qt bug
#if defined(Q_OS_MAC)
    ui->minButton->setVisible(false);
    ui->minmaxButton->setVisible(false);
#endif

    //--add a dropshadow to our form
    this->setAttribute(Qt::WA_TranslucentBackground); //hide main window
    effect = new QGraphicsDropShadowEffect();
    effect->setBlurRadius(15);
    effect->setColor(QColor("#000000"));
    effect->setOffset(QPoint(effect->xOffset() - 2, effect->yOffset() - 2));
    ui->shadower->setGraphicsEffect(effect);
    ui->shadower->setStyleSheet("#shadower { background-color: #6F2E50; color: #F5F5F5;}");

    //--set up how we want our frameless window
    framelessHelper = new NcFramelessHelper;
    framelessHelper->activateOn(this);
    framelessHelper->setWidgetMovable(true);
    framelessHelper->setWidgetResizable(true);

    //--set up events
    ui->input_text->installEventFilter(this);

    //--add a tool tip to [Chatroom] (our only element thus far) and add it to our "chats"
    for (int j = 0; j < ui->userList->count(); ++j)
    {
        QListWidgetItem *item = ui->userList->item(j);
        QString indexName = item->text();
        item->setToolTip(indexName);

        if (indexName == defaultChat)
        {
            //the default is [Chatroom]. let's give it a welcome message
            QString msg = "Welcome to the <strong>Chatroom</strong>!";
            userList.insert(indexName, msg);
            item->setSelected(true); //have this selected by default
            //break;
        }

        //this is a case that happens if we decide to have more than [Chatroom] as our default user list
        else
        {
            QString msg = "You are now chatting with <strong>" + indexName + "</strong>!";
            userList.insert(indexName, msg);
        }
    }
    //--set up the default of what interact display should show
    ui->interactDisplay->setText(userList.value(defaultChat));

    //--default don't show font menu
    ui->fontMenu->setVisible(false);
}

Chatroom::~Chatroom()
{
    delete tcpSocket;
    delete effect;
    delete framelessHelper;
    delete ui;
}

void Chatroom::setUsername(const QString &name)
{
    username = name;
}

void Chatroom::setIPandPort(const QString &ip, int port)
{
    targetIpAddress = ip;
    targetPort = port;
    tcpSocket->connectToHost(targetIpAddress, targetPort);
    if (tcpSocket->waitForConnected(2000))
    {
        sendInput("first send");
    }
}

void Chatroom::on_userList_clicked(const QModelIndex &index)
{
    QListWidgetItem *item = ui->userList->item(index.row());

    //reset the bg color (for if it was "not selected" but there was an update, the bg would be set)
    item->setBackgroundColor(QColor("#F5F5F5"));

    //get the "text" back from the userList. QModelIndex stores the row inside itself
    //chatDex is then the name of the user that was clicked on the list (this user's chat PARTNER)
    QString chatDex = item->text();

    //load up the chat
    ui->interactDisplay->setText(userList.value(chatDex));

    //save who we're currently chatting with
    currentChat = chatDex;
}

void Chatroom::updateUserList(const QVector<QString> &users)
{
    int size = ui->userList->count();
    int uSize = users.size();

    //users.size + 1 is to account for the default [Chatroom]
    if (size != uSize + 1)
    {
        //iterate through our list of users and remove the people that no longer exist
        for (int row = size - 1; row >= 0; --row)
        {
            QListWidgetItem *item = ui->userList->item(row);
            QString indexName = item->text();

            if (!users.contains(oinker.encrypt(indexName)) && indexName != defaultChat)
            {
                //qDebug() << "current: " << indexName;

                //remove from the listWidget
                ui->userList->takeItem(row);

                //remove from the userList
                userList.remove(indexName);

                //--if we were just talking to a person who left, display the defaultChat
                if (indexName == currentChat)
                {
                    ui->userList->item(0)->setSelected(true); //highlight the chatroom
                    ui->interactDisplay->setText(userList.value(defaultChat));
                }
            }
        }

        //now add the people who are new into our chatroom list
        QVectorIterator<QString> i(users);
        while (i.hasNext())
        {
            QString user = oinker.decrypt(i.next());

            if (!userList.contains(user))
            {
                QListWidgetItem *item = new QListWidgetItem();
                item->setText(user);
                item->setToolTip(user);

                //add to the listWidget
                ui->userList->addItem(item);

                //add to our userList (of names and chat history)
                QString msg = "You are now chatting with <strong>" + user + "</strong>!";
                userList.insert(user, msg);
            }
        }
    }

    //caveat, you can chat with yourself because of the above algorithm (because the users will contain you as well)
}

QString Chatroom::processInput(const QString &input)
{
    QString ret = "";

    //work with the string if it's not empty
    if (!input.isEmpty())
    {
        QString s = input;

        //--strip the html that the user might have tried to enter themselves. only we should add html, not them
        s.remove(QRegExp("<[^>]*>+"));
        if(s.isEmpty()){
            ui->input_text->setPlaceholderText("Please do not insert scripts or html. (No <...>)");
            return ret;
        }

        //--if they tried to copy and paste text that have crlf, we must replace them with linebreaks (html style)
        s.replace(QRegExp("[\r\n]+"), "<br/>");

        //--don't allow them to enter so much text (trim it)
        int maxLen = 500;
        s = s.mid(0, maxLen);

        //--add in the user's specified font/color
        QString font = ui->fontEdit->text();
        QString color = ui->colorEdit->text();

        ret = "<font face=\"" + font + "\" color=\"" + color + "\">" + s + "</font>";
    }

    return ret;
}

void Chatroom::sendInput(const QString &input)
{
    QString s = processInput(input);

    //only append if it's not empty string
    if (!s.isEmpty())
    {
        QString sender = oinker.encrypt(username); //this is who we are
        QString receiver = oinker.encrypt(currentChat); //the currentChat is the person we're talking to
        QString msg = oinker.encrypt(s.toUtf8());

        OinkObject oinkObject;
        oinkObject.setFlag("send");
        oinkObject.setMessage(MessageObject(msg, sender, receiver));

        //qDebug() << "sends " << sender << " " << receiver << " " << msg;

        // send back the oink object whose Ack has been set
        QByteArray block; // temporarily stores the data we are writing
        QDataStream out(&block, QIODevice::WriteOnly); // writes in data
        out.setVersion(QDataStream::Qt_5_2);  // version needs to match OinkClient's
        out << oinkObject;
        tcpSocket->write(block);
        tcpSocket->flush();
    }

    ui->input_text->clear();
    //move the cursor back to the beginning
    QTextCursor cursor(ui->input_text->textCursor());
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);
}

void Chatroom::updateChat(const QString &sender, const QString &receiver, QString &message)
{
    QString destination = "";

    //--need to check receiver.  if receiver == [Chatroom] that means the message is for chatroom
    if (receiver.compare(defaultChat) == 0)
    {
        destination = defaultChat;
    }
    else if (sender.compare(username) == 0)
    {
        //--if sender == this user, that means it's a private chat. so it should go to the private chat with the receiver (the server sends back the exact copy!)
        destination = receiver;
    }
    else
    {
        //--so if I didn't send the message, and it's not a message for the chatroom, then this must've been a private message from someone else
        destination = sender;
    }

    //--check the length of the history so far.  (we want to clear it or we'll lag when we load each time)
    int maxLen = 10000;
    int len = userList[destination].length();
    if (len > maxLen)
    {
        userList[destination].clear();
        userList[destination].append("<strong>Previous conversation history was removed.</strong>");
    }

    //--make sure it's not a message from the server before we append the username
    if (sender.compare("[Server]") != 0)
    {
        //--prepend the sender to the message (bolded)
        message.prepend("<strong>" + sender + "</strong>: ");
    }

    //--prepend a "newline" so everything is on it's own line
    userList[destination].append("<br/>" + message); //can't use \n because textEdit reads html

    //--update interactDisplay only if it's appending to the current chat
    if (destination == currentChat)
    {

        if (soundSwitch)
        {
            //play a sound
            QSound::play(":/src/resource/sounds/type2.wav");
        }

        if (len > maxLen)
        {
            ui->interactDisplay->setText(userList[destination]); //need to use setText because we removed from the chat history
        }
        else
        {
            ui->interactDisplay->append(message); //use append instead of set text or else that's a big hit on performance
        }
    }
    else
    {
        //there's an update in one of our other chats, so set a highlight on them
        //must use a loop because there's no findbylabel function
        int size = ui->userList->count();
        for (int row = size - 1; row >= 0; --row)
        {
            QListWidgetItem *item = ui->userList->item(row);
            QString indexName = item->text();

            if (destination == indexName)
            {
                //set a highlight on them
                ui->userList->item(row)->setBackgroundColor(QColor("#D1D3D4"));
            }
        }
    }
}


// invoked when QIODevice() emits readyRead() signal
void Chatroom::readMessage()
{
    while (tcpSocket->bytesAvailable())
    {
        QDataStream in(tcpSocket);
        OinkObject oinkObject;
        in >> oinkObject;
        QString flag = oinkObject.getFlag();

        QString sender = oinker.decrypt(oinkObject.getMessage().getSender());
        QString receiver = oinker.decrypt(oinkObject.getMessage().getReceiver());
        QString msg = oinker.decrypt(oinkObject.getMessage().getMessage());

        //qDebug() << "gets Read";
        //qDebug() << flag;
        //qDebug() << sender;
        //qDebug() << receiver;
        //qDebug() << msg;

        if (flag == "send")
        {
            //this will update the GUI
            updateChat(sender, receiver, msg);
        }

        else if (flag == "update")
        {
            updateChat(sender, receiver, msg);
            updateUserList(oinkObject.getUserList());
        }
    }
}


void Chatroom::on_quitButton_clicked()
{
    tcpSocket->close();
    this->close();
}

void Chatroom::on_minButton_clicked()
{
    this->showMinimized();
}

void Chatroom::on_minmaxButton_clicked()
{
    if (minmaxSwitch)
    {
        this->showMaximized();
        minmaxSwitch = false;
    }
    else
    {
        this->showNormal();
        minmaxSwitch = true;
    }
}


void Chatroom::on_enter_button_clicked()
{
    sendInput(ui->input_text->toPlainText());
}

bool Chatroom::eventFilter(QObject *object, QEvent *event)
{

    if (event->type() == QEvent::KeyPress && object == ui->input_text)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        ui->input_text->setPlaceholderText("");
        if (keyEvent->key() == Qt::Key_Return || keyEvent->key() == Qt::Key_Enter)
        {
            sendInput(ui->input_text->toPlainText());
            return true;
        }
    }

    return false;
}



void Chatroom::on_soundButton_clicked()
{
    if (soundSwitch)
    {
        ui->soundButton->setStyleSheet("background: #6F2E50 url(:/src/resource/nosound.png) no-repeat center;");
        soundSwitch = false;
    }
    else
    {
        ui->soundButton->setStyleSheet("background: #6F2E50 url(:/src/resource/sound.png) no-repeat center;");
        soundSwitch = true;
    }
}

void Chatroom::on_fontButton_clicked()
{
    if (fontMenuSwitch)
    {
        ui->fontMenu->setVisible(true);
        fontMenuSwitch = false;
    }
    else
    {
        ui->fontMenu->setVisible(false);
        fontMenuSwitch = true;
    }
}

void Chatroom::on_resetButton_clicked()
{
    ui->fontEdit->clear();
    ui->colorEdit->clear();
}
