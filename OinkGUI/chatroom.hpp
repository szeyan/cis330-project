#ifndef CHATROOM_HPP
#define CHATROOM_HPP

#include <QtCore>
#include <QtNetwork>
#include <QDialog>
#include <QTcpServer>
#include <QTcpSocket>
#include <QtCore>
#include <QGraphicsDropShadowEffect>
#include <QEvent>
#include "NcFramelessHelper/src/NcFramelessHelper.h"
#include "oinkconfigurator.hpp"

namespace Ui
{
class Chatroom;
}

/**
 * The chatroom gui is responsible for showing a
 * chatroom interface of between all connected users
 *
 * This class keeps track of ChatPrivate objects that are constructed.
 *
 * @author SzeYan Li for GUI elements
 * @author Hanxiao Zhang, Xi Zhang, Xinwei Cao for back-end networking
 */
class Chatroom : public QDialog
{
        Q_OBJECT

    public:
        /**
         * Constructor
         */
        explicit Chatroom(QWidget *parent = 0);
        /**
         * Destructor
         */
        ~Chatroom();

        /**
         * Sets the name of this user
         * @param name username of this user
         */
        void setUsername(const QString &name);

        /**
         * Sets the ip and port that the chatroom should connect to
         * @param ip the IP address to connect to
         * @param port the port to connect to
         */
        void setIPandPort(const QString &ip, int port);

    public slots:
        /**
         * Reads a message from the server
         * Connected to QTcpSocket readyRead() signal
         */
        void readMessage();

    private slots:
        /**
         * Opens up a chat with the specified username (by the index)
         * @param index the index in the usersList
         */
        void on_userList_clicked(const QModelIndex &index);

        /**
         * When the enter button is clicked, the text inside the input_text box is sent to its destination
         */
        void on_enter_button_clicked();

        /**
         * Simulates the quit button
         */
        void on_quitButton_clicked();

        /**
         * Simulates the minimize button
         */
        void on_minButton_clicked();

        /**
         * Simulates the min/max button
         */
        void on_minmaxButton_clicked();

        /**
         * Filters special events
         * Grabs the enter button keypress from the input_text box and sends it to its destination
         */
        bool eventFilter(QObject *object, QEvent *event);

        /**
         * Toggles sound on/off
         */
        void on_soundButton_clicked();

        /**
         * Toggles show font menu on/off
         */
        void on_fontButton_clicked();

        /**
         * Toggles to reset the font and colors of the chat
         */
        void on_resetButton_clicked();

    private:
        //Networking variables
        QTcpSocket *tcpSocket; ///< the socket connected to the server
        QString targetIpAddress; ///< the ip address to connect to
        int targetPort; ///< the port to connect to

        //GUI variables
        Ui::Chatroom *ui; ///< The gui connected to this class
        OinkConfigurator oinker; ///< a configurator for fonts/styles/encrypt/decrypt
        bool minmaxSwitch; ///< used by on_minmaxButton_clicked() to know when to minimize or maximize
        NcFramelessHelper *framelessHelper; ///< used to create frameless gui window
        QGraphicsDropShadowEffect *effect; ///< used to create a dropshadow effect for the gui

        //Chatroom variables
        QHash<QString, QString> userList; ///< key = username. value = history of conversation
        QString defaultChat; ///< the default chat is "[Chatroom]"
        QString currentChat; ///< the name of the current chat instance that is opened
        QString username; ///< username of this user
        bool soundSwitch; ///< toggles sound on/off
        bool fontMenuSwitch; ///< toggles font menu on/off

        /**
         * Helper function for initializing the gui elements
         */
        void initGUI();

        /**
         * Helper function for initializing the gui elements
         * @param users current list of active users
         */
        void updateUserList(const QVector<QString> &users);

        /**
         * Updates the chat history and the GUI
         * @param sender the sender of the message
         * @param receiver the receiver of the message
         * @param message the message the sender sent
         */
        void updateChat(const QString &sender, const QString &receiver, QString &message);

        /**
         * Sends the given input to the server
         * @param input string to append
         */
        void sendInput(const QString &input);

        /**
         * Processes an input.
         *      -strips away crlf and replaces them with <br/> tags
         *      -strips away user's inputted html/script tags
         *      -adds font/color specified by user
         * @param input string process
         * @return the processed string
         */
        QString processInput(const QString &input);

};

#endif // CHATROOM_HPP
