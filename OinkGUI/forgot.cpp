#include "forgot.hpp"
#include "ui_forgot.h"
#include "accinfo.hpp"
#include "about.hpp"
#include <QTimer>
#include "Protocol/oinkobject.hpp"


Forgot::Forgot(QWidget *parent) :
    QDialog(parent), ui(new Ui::Forgot),
    framelessHelper(NULL), effect(NULL),
    username(""), password(""), email(""),
    targetIpAddress("127.0.0.1"), targetPort(252525)
{
    //--load up GUI elements
    initGUI();

    //--set up the socket
    authenSocket = new QTcpSocket(this);
    connect(authenSocket, SIGNAL(readyRead()), this, SLOT(verification()));
}

void Forgot::initGUI()
{
    //--we want no frames and we want our own custom shadows
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    flags |= Qt::NoDropShadowWindowHint;
    this->setWindowFlags(flags);

    //--add a context menu
    QAction *quitAction = new QAction(tr("E&xit"), this);
    quitAction->setObjectName("rightcontextmenu");
    quitAction->setShortcut(tr("Ctrl+Q"));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    addAction(quitAction);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    //--set up the ui (must be set up before setting the font for widgets)
    ui->setupUi(this);
    this->setWindowTitle("");

    //--set style sheet
    this->setStyleSheet(oinker.getStyleSheet());

    //--set the fonts for the labels/widgets (because macs can't load custom fonts, we'll just load images
#if defined(Q_OS_WIN) || defined(Q_OS_UNIX) //tested to be working on windows and unix at least
    oinker.loadFonts();
    ui->email_label->setFont(oinker.getUbuntuCondensed());
    ui->forgot_title->setFont(oinker.getUbuntuLight());
    ui->credit_label->setFont(oinker.getUbuntuRegular());
    ui->submit_button->setFont(oinker.getUbuntuCondensed());
    ui->cancel_button->setFont(oinker.getUbuntuCondensed());
#endif

#if defined(Q_OS_MAC) || (!defined(Q_OS_WIN) && !defined(Q_OS_UNIX))
    ui->email_label->setText("");
    ui->email_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/email.png) no-repeat left center;");
    ui->submit_button->setText("");
    ui->submit_button->setStyleSheet("background: #FDB71F url(:/src/resource/macsupport/submit.png) no-repeat center;");
    ui->cancel_button->setText("");
    ui->cancel_button->setStyleSheet("background: #FDB71F url(:/src/resource/macsupport/cancel.png) no-repeat center;");
#endif

    //--add a dropshadow to our form
    this->setAttribute(Qt::WA_TranslucentBackground); //hide main window
    effect = new QGraphicsDropShadowEffect();
    effect->setBlurRadius(15);
    effect->setColor(QColor("#000000"));
    effect->setOffset(QPoint(effect->xOffset() - 2, effect->yOffset() - 2));
    ui->shadower->setGraphicsEffect(effect);
    ui->shadower->setStyleSheet("#shadower { background-color: #6F2E50; color: #F5F5F5;}");

    //--set up how we want our frameless window
    framelessHelper = new NcFramelessHelper;
    framelessHelper->activateOn(this);
    framelessHelper->setWidgetMovable(true);
    framelessHelper->setWidgetResizable(true);

    //--we'll show this when forgot form fails
    ui->responseLabel->setStyleSheet("#responseLabel { color: #6F2E50; font-size:10pt; }");
}

Forgot::~Forgot()
{
    delete effect;
    delete framelessHelper;
    delete ui;
    delete authenSocket;
}

void Forgot::setIPandPort(const QString &ip, int port)
{
    targetIpAddress = ip;
    targetPort = port;
}

void Forgot::on_cancel_button_clicked()
{
    this->close();
}

bool Forgot::on_submit_button_clicked()
{
    //--reset the response label. responseLabel is used for displaying a message if user failed to login.
    ui->responseLabel->setStyleSheet("#responseLabel { color: #6F2E50; font-size:10pt; }");

    //--first check if the email address is valid
    if (!oinker.isValidEmail(email))
    {
        //--display error message
        ui->responseLabel->setText("Invalid email format.");
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        shakeGUI();
        return false;
    }

    //--set the oink object
    OinkObject o;
    o.setFlag("forgotAcc");
    o.setUser(UserObject("", "", oinker.encrypt(email)));

    //--read oink object into datastream
    QByteArray block; // temporarily stores the data we are writing
    QDataStream out(&block, QIODevice::WriteOnly); // writes in data
    out.setVersion(QDataStream::Qt_5_2);  // version needs to match OinkClient's
    out << o;

    //--connect and send
    authenSocket->connectToHost(targetIpAddress, targetPort);

    //--check for timeout
    if (!authenSocket->waitForConnected(2000))
    {
        qDebug() << "Error: " << authenSocket->errorString();
        ui->responseLabel->setText("Could not connect to server.");
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        shakeGUI();
        authenSocket->close();
        return false;
    }

    authenSocket->write(block);

    return true;

}

void Forgot::verification()
{

    //--retrieve the oink object
    QDataStream in(authenSocket);
    OinkObject oinkObject;
    in >> oinkObject;
    QString flag = oinkObject.getFlag();

    //--close the socket no matter what
    authenSocket->close();

    if (flag == "forgotAcc")
    {
        if (oinkObject.isAck())
        {
            QString username = oinker.decrypt(oinkObject.getUser().getName());
            QString password = oinker.decrypt(oinkObject.getUser().getPassword());

            //--reponse message
            ui->responseLabel->setText("An email was also sent with your account details.");
            ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");

            //--send an email to the user with their account details as well
            QString msg = "Your username is " + username + "\nYour password is " + password;
            QString header = "Account Details for OinkChat";
            oinker.sendEmail(email, header, msg);

            //--show accinfo
            AccInfo accinfo;
            accinfo.setUsername(username);
            accinfo.setPassword(password);
            accinfo.setModal(true);
            accinfo.exec();
        }
        else
        {
            //--display error message
            ui->responseLabel->setText("User account not found.");
            ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
            shakeGUI();
        }
    }

}

void Forgot::on_credit_label_clicked()
{
    //--show the about box
    About about;
    about.setModal(true);
    about.exec();
}


void Forgot::on_email_edit_editingFinished()
{
    email = ui->email_edit->text();
}

void Forgot::shakeGUI()
{

    //we only want this to shake on mac or windows, since it doesn't work with unix
#if defined(Q_OS_WIN) || defined(Q_OS_MAC)

    static int numTimesCalled = 0;
    static bool shakeSwitch = false;
    QPoint offset(10, 0);

    //--incre times called
    numTimesCalled++;

    //--max calls to self is 9 times
    if (numTimesCalled == 9)
    {
        numTimesCalled = 0;
        return;
    }

    //--move it either left or right depending on the shake switch
    move(((shakeSwitch) ? pos() + offset : pos() - offset));

    //--alternate the direction
    if (shakeSwitch)
    {
        shakeSwitch = false;
    }
    else
    {
        shakeSwitch = true;
    }

    //--shake for 40 msec
    QTimer::singleShot(40, this, SLOT(shakeGUI()));

#endif
}

