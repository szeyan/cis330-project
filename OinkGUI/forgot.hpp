#ifndef FORGOT_HPP
#define FORGOT_HPP

#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsDropShadowEffect>
#include "NcFramelessHelper/src/NcFramelessHelper.h"
#include "oinkconfigurator.hpp"


namespace Ui
{
class Forgot;
}

/**
 * The forgot GUI is responsible retrieving a user's account information
 *
 * @author SzeYan Li for GUI elements
 * @author Hanxiao Zhang for networking
 */
class Forgot : public QDialog
{
        Q_OBJECT

    public:
        /**
         * Constructor
         */
        explicit Forgot(QWidget *parent = 0);
        /**
         * Destructor
         */
        ~Forgot();

        /**
         * Sets the ip and port that the chatroom should connect to
         * @param ip the IP address to connect to
         * @param port the port to connect to
         */
        void setIPandPort(const QString &ip, int port);

    private slots:
        /**
         * Closes this window and goes back to the login screen
         */
        void on_cancel_button_clicked();

        /**
         * Submits the email inputted to the server to retrieve user info
         * @return true if able to retrieve account details, false if not
         */
        bool on_submit_button_clicked();

        /**
         * Opens up the about gui
         */
        void on_credit_label_clicked();

        /**
         * Copies the email from the GUI when the user is done typing
         */
        void on_email_edit_editingFinished();

        /**
         * Shakes the GUI (only on Windows and Mac)
         * reference: //http://stackoverflow.com/questions/10677324/possible-to-make-qdialog-shake-in-osx
         */
        void shakeGUI();

        /**
         * Receive a feedback from the server to see if this user exists
         */
        void verification();

    private:
        //GUI variables
        Ui::Forgot *ui; ///< The gui connected to this class
        OinkConfigurator oinker; ///< a configurator for fonts/styles/encrypt/decrypt
        NcFramelessHelper *framelessHelper; ///< used to create frameless gui window
        QGraphicsDropShadowEffect *effect; ///< used to create a dropshadow effect for the gui

        //Forgot-form variables
        QString username; ///< the username of this user
        QString password; ///< the password of this user
        QString email; ///< the email of this user

        //Networking variables
        QString targetIpAddress; ///< the ip address to connect to
        QTcpSocket *authenSocket; ///< the socket connected to the server
        int targetPort; ///< the port to connect to

        /**
         * Helper function for initializing the gui elements
         */
        void initGUI();

};

#endif // FORGOT_HPP
