#include <QFile>
#include <QString>
#include <QtCore>
#include <QTimer>
#include <QSound>
#include <QDebug>
#include <QGraphicsDropShadowEffect>

#include "login.hpp"
#include "ui_login.h"
#include "about.hpp"
#include "register.hpp"
#include "chatroom.hpp"
#include "forgot.hpp"
#include "Protocol/oinkobject.hpp"

Login::Login(QWidget *parent) :
    QDialog(parent), ui(new Ui::Login),
    minmaxSwitch(true), framelessHelper(NULL), effect(NULL),
    username(""), password(""),
    targetIpAddress("127.0.0.1"), targetPort(252525)
{
    //--load up GUI elements
    initGUI();

    //--set up the socket
    authenSocket = new QTcpSocket(this);
    connect(authenSocket, SIGNAL(readyRead()), this, SLOT(verification()));
}

void Login::initGUI()
{
    //--we want no frames and we want our own custom shadows
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    flags |= Qt::NoDropShadowWindowHint;
    this->setWindowFlags(flags);

    //--add a context menu
    QAction *quitAction = new QAction(tr("E&xit"), this);
    quitAction->setObjectName("rightcontextmenu");
    quitAction->setShortcut(tr("Ctrl+Q"));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    addAction(quitAction);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    //--set up the ui (must be set up before setting the font for widgets)
    ui->setupUi(this);
    this->setWindowTitle("");

    //--set style sheet
    this->setStyleSheet(oinker.getStyleSheet());

    //--set the fonts for the labels/widgets (because macs can't load custom fonts, we'll just load images
#if defined(Q_OS_WIN) || defined(Q_OS_UNIX) //tested to be working on windows and unix at least
    oinker.loadFonts();
    ui->title_label->setFont(oinker.getFontRumpel());
    ui->username_label->setFont(oinker.getUbuntuCondensed());
    ui->pwd_label->setFont(oinker.getUbuntuCondensed());
    ui->register_button->setFont(oinker.getUbuntuCondensed());
    ui->login_button->setFont(oinker.getUbuntuCondensed());
    ui->forgot_label->setFont(oinker.getUbuntuLight());
    ui->remember_box->setFont(oinker.getUbuntuLight());
    ui->credit_label->setFont(oinker.getUbuntuRegular());
    ui->responseLabel->setFont(oinker.getUbuntuLight());
#endif

#if defined(Q_OS_MAC) || (!defined(Q_OS_WIN) && !defined(Q_OS_UNIX))
    ui->title_label->setText("");
    ui->title_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/oink.png) no-repeat center;");
    ui->username_label->setText("");
    ui->username_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/username.png) no-repeat left center;");
    ui->pwd_label->setText("");
    ui->pwd_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/password.png) no-repeat left center;");
    ui->register_button->setText("");
    ui->register_button->setStyleSheet("background: #FDB71F url(:/src/resource/macsupport/register.png) no-repeat center;");
    ui->login_button->setText("");
    ui->login_button->setStyleSheet("background: #FDB71F url(:/src/resource/macsupport/login.png) no-repeat center;");
    ui->forgot_label->setText("");
    ui->forgot_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/forgot_label.png) no-repeat left center;");
#endif

//bug report: showMinimized and showMaximized doesn't work on Macs. It's a Qt bug
#if defined(Q_OS_MAC)
   ui->minButton->setVisible(false);
   ui->minmaxButton->setVisible(false);
#endif

    //--load config file
    loadConfig();

    //--add a dropshadow to our form
    this->setAttribute(Qt::WA_TranslucentBackground); //hide main window
    effect = new QGraphicsDropShadowEffect();
    effect->setBlurRadius(15);
    effect->setColor(QColor("#000000"));
    effect->setOffset(QPoint(effect->xOffset() - 2, effect->yOffset() - 2));
    ui->shadower->setGraphicsEffect(effect);
    ui->shadower->setStyleSheet("#shadower { background-color: #6F2E50; color: #F5F5F5;}");

    //--set up how we want our frameless window
    framelessHelper = new NcFramelessHelper;
    framelessHelper->activateOn(this);
    framelessHelper->setWidgetMovable(true);
    framelessHelper->setWidgetResizable(true);

    //--we'll show this response message upon user interaction
    ui->responseLabel->setStyleSheet("#responseLabel { color: #6F2E50; font-size:10pt; }");
}


Login::~Login()
{
    delete authenSocket;
    delete effect;
    delete framelessHelper;
    delete ui;
}

void Login::on_credit_label_clicked()
{
    //--show the about box
    About about;
    about.setModal(true);
    about.exec();
}

void Login::setIPandPort(const QString &ip, int port)
{
    targetIpAddress = ip;
    targetPort = port;
}


void Login::on_register_button_clicked()
{
    //--show register
    Register reg;
    reg.setIPandPort(targetIpAddress, targetPort);
    reg.setModal(true);
    reg.exec();
}

bool Login::on_login_button_clicked()
{
    //reset the response label. responseLabel is used for displaying a message if user failed to login.
    ui->responseLabel->setStyleSheet("#responseLabel { color: #6F2E50; font-size:10pt; }");

    //--update our config file if remember me is checked
    writeConfig();

    //--validate that username and password isn't empty
    if (!oinker.isValidUsername(username) || !oinker.isValidPassword(password))
    {
        //--display error message
        ui->responseLabel->setText("Invalid username or password.");
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        shakeGUI();
        return false;
    }

    //--set the oink object
    OinkObject o;
    o.setFlag("login");
    o.setUser(UserObject(oinker.encrypt(username), oinker.encrypt(password), ""));

    //--read oink object into datastream
    QByteArray block; // temporarily stores the data we are writing
    QDataStream out(&block, QIODevice::WriteOnly); // writes in data
    out.setVersion(QDataStream::Qt_5_2);  // version needs to match OinkClient's
    out << o;

    //--connect and send
    authenSocket->connectToHost(targetIpAddress, targetPort);


    //--check for timeout
    if (!authenSocket->waitForConnected(2000))
    {
        qDebug() << "Error: " << authenSocket->errorString();
        ui->responseLabel->setText("Could not connect to server.");
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        shakeGUI();
        authenSocket->close();
        return false;
    }

    authenSocket->write(block);

    return true;
}

void Login::verification()
{
    //--retrieve the oink object
    QDataStream in(authenSocket);
    OinkObject oinkObject;
    in >> oinkObject;
    QString flag = oinkObject.getFlag();

    //--close the socket no matter what
    authenSocket->close();

    //--process flag
    if (flag == "login")
    {
        if (oinkObject.isAck())
        {
            //--close this login window the socket
            this->close();

            //--show the chatroom gui
            Chatroom chatrm;
            chatrm.setUsername(username);
            chatrm.setIPandPort(targetIpAddress, targetPort);
            chatrm.setModal(true);
            chatrm.exec();
        }
        else
        {
            //--login failed
            ui->responseLabel->setText(oinker.decrypt(oinkObject.getMessage().getMessage()));
            ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
            shakeGUI();
        }

    }
}



void Login::on_forgot_label_clicked()
{
    //show forgot gui form
    Forgot forgot;
    forgot.setIPandPort(targetIpAddress, targetPort);
    forgot.setModal(true);
    forgot.exec();
}

void Login::writeConfig()
{
    //we want to write regardless if remember me is checked
    //because if they no longer want the app to remember the un and pwd,
    //oinkchat can wipe out the config file

    //--obtain the values from the gui
    bool checked = ui->remember_box->isChecked();

    //--getting current path of exe
    QString path = QCoreApplication::applicationDirPath();

    //--load file
    QFile file(path + "/.config");

    if (!file.open(QIODevice::WriteOnly))
    {
        //can't open the file
        qDebug() << "Warning: Could not open or find the Config File";
    }
    else
    {
        qDebug() << "Wrote Config File: " + path + "/.config";
        QDataStream out(&file);
        out.setVersion(QDataStream::Qt_5_2);

        //--write values
        QString enc_un = oinker.encrypt(username);
        QString enc_pwd = oinker.encrypt(password);

        if (checked)
        {
            out << checked;
            out << enc_un;
            out << enc_pwd;

            //qDebug() << "checked  = " << checked;
            //qDebug() << "username = " << enc_un;
            //qDebug() << "password = " << enc_pwd;
        }
        else
        {
            out << false;
        }

        file.flush();
        file.close();
    }
}

void Login::loadConfig()
{
    bool checked = false;

    //--getting current path of exe
    QString path = QCoreApplication::applicationDirPath();

    //--load file
    QFile file(path + "/.config");
    qint32 fileSize = (qint32) file.size(); //this is in bytes
    qint32 maxSize = 1024; //simple check for outside modifications of config file
    if (!file.open(QIODevice::ReadOnly) || fileSize > maxSize)
    {
        //can't open the file
        qDebug() << "Warning: Could not open or find the Config File";
    }
    else
    {
        qDebug() << "Loaded Config File: " + path + "/.config";
        QDataStream in(&file);
        in.setVersion(QDataStream::Qt_5_2);

        in >> checked;

        //only config if "remember me" was recorded as "checked"
        if (checked)
        {
            in >> username;
            in >> password;
            file.close();

            //simple security check for modifications
            //if the password or the username is nothing, don't get them
            if (username.length() == 0 || password.length() == 0)
            {
                //this works because if you try to change a character in the config file,
                //the QDataStream usually cannot read in one of the values
                qDebug() << "Could not read username or password";
            }
            else
            {
                ui->remember_box->setChecked(checked);

                username = oinker.decrypt(username);
                password = oinker.decrypt(password);

                ui->username_edit->setText(username);
                ui->pwd_edit->setText(password);

                //qDebug() << "checked  = " << checked;
                //qDebug() << "username = " << username;
                //qDebug() << "password = " << password;
            }
        }
    }
}

void Login::on_quitButton_clicked()
{
    this->close();
}

void Login::on_minButton_clicked()
{
    this->showMinimized();

}

void Login::on_minmaxButton_clicked()
{
    if (minmaxSwitch)
    {
        this->showMaximized();
        minmaxSwitch = false;
    }
    else
    {
        this->showNormal();
        minmaxSwitch = true;
    }
}

void Login::on_username_edit_editingFinished()
{
    username = ui->username_edit->text();
}

void Login::on_pwd_edit_editingFinished()
{
    password = ui->pwd_edit->text();
}

void Login::shakeGUI()
{

    //we only want this to shake on mac or windows, since it doesn't work with unix
#if defined(Q_OS_WIN) || defined(Q_OS_MAC)

    static int numTimesCalled = 0;
    static bool shakeSwitch = false;
    QPoint offset(10, 0);

    //--incre times called
    numTimesCalled++;

    //--max calls to self is 9 times
    if (numTimesCalled == 9)
    {
        numTimesCalled = 0;
        return;
    }

    //--move it either left or right depending on the shake switch
    move(((shakeSwitch) ? pos() + offset : pos() - offset));

    //--alternate the direction
    if (shakeSwitch)
    {
        shakeSwitch = false;
    }
    else
    {
        shakeSwitch = true;
    }

    //--shake for 40 msec
    QTimer::singleShot(40, this, SLOT(shakeGUI()));

#endif
}
