#ifndef LOGIN_H
#define LOGIN_H

#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QTcpSocket>
#include <QGraphicsDropShadowEffect>
#include "NcFramelessHelper/src/NcFramelessHelper.h"
#include "oinkconfigurator.hpp"

namespace Ui
{
class Login;
}

/**
 * The login GUI is responsible for user login
 * It is the first interface the user sees
 * Can redirect the user to:
 *      -the chatroom
 *      -the forgot username/password form
 *      -the about popup
 *      -the registration form
 *
 * @author SzeYan Li for GUI elements
 * @author Hanxiao Zhang for networking
 */
class Login : public QDialog
{
        Q_OBJECT

    public:
        /**
         * Constructor
         */
        explicit Login(QWidget *parent = 0);

        /**
         * Destructor
         */
        ~Login();

        /**
         * Sets the ip and port that the whole client should connect to
         * @param ip the IP address to connect to
         * @param port the port to connect to
         */
        void setIPandPort(const QString &ip, int port);

    private slots:
        /**
         * Opens up the about gui
         */
        void on_credit_label_clicked();

        /**
         * Opens up the registration form
         */
        void on_register_button_clicked();

        /**
         * Opens up the chatroom (if connection successful)
         * @return true, if can login.  false if not.
         */
        bool on_login_button_clicked();

        /**
         * Opens up the forgot username and password form
         */
        void on_forgot_label_clicked();

        /**
         * Simulates the quit button
         */
        void on_quitButton_clicked();

        /**
         * Simulates the minimize button
         */
        void on_minButton_clicked();

        /**
         * Simulates the min/max button
         */
        void on_minmaxButton_clicked();

        /**
         * Copies the username from the GUI when the user is done typing
         */
        void on_username_edit_editingFinished();

        /**
         * Copies the password from the GUI when the user is done typing
         */
        void on_pwd_edit_editingFinished();

        /**
         * Receive a feedback from the server to see if this user exists
         */
        void verification();

        /**
         * Shakes the GUI (only on Windows and Mac)
         * reference: //http://stackoverflow.com/questions/10677324/possible-to-make-qdialog-shake-in-osx
         */
        void shakeGUI();

    private:
        //GUI variables
        Ui::Login *ui; ///< The gui connected to this class
        OinkConfigurator oinker; ///< a configurator for fonts/styles/encrypt/decrypt
        bool minmaxSwitch; ///< used by on_minmaxButton_clicked() to know when to minimize or maximize
        NcFramelessHelper *framelessHelper; ///< used to create frameless gui window
        QGraphicsDropShadowEffect *effect; ///< used to create a dropshadow effect for the gui

        //Login variables
        QString username; ///< the username of this user
        QString password; ///< the password of this user

        //Networking variables
        QTcpSocket *authenSocket; ///< the socket connected to the server
        QString targetIpAddress; ///< the ip address to connect to
        int targetPort; ///< the port to connect to

        /**
         * Helper function for initializing the gui elements
         */
        void initGUI();

        /**
         * Loads a configuration file
         */
        void loadConfig();
        /**
         * writes a configuration file
         */
        void writeConfig();

};

#endif // LOGIN_H
