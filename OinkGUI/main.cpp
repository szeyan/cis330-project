#include <QString>
#include "login.hpp"
#include <QApplication>

int main(int argc, char *argv[])
{
    qDebug() << "Compiled with Qt Version: " << QT_VERSION_STR;
    QApplication oinkChat(argc, argv);

    QString projectPath(PROJECT_PATH);
    QString buildPath(BUILD_PATH);
    qDebug() << "Project path: " << projectPath;
    qDebug() << "Build path: " << buildPath;

    //--set the app icon if it's not windows
#ifndef Q_OS_WIN
    QIcon icon(":/src/resource/trans_pig.png");
    oinkChat.setWindowIcon(icon);
#endif

    //--set the app name
    oinkChat.setApplicationDisplayName("Oink Chat");
    oinkChat.setApplicationName("Oink Chat");

    //--set ip and port
    QString ip = "10.111.78.219";
    int port = 252525;

    //--display the first gui
    Login login;
    login.setIPandPort(ip, port);
    login.show();

    return oinkChat.exec();
}
