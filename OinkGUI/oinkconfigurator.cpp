#include <QFile>
#include <QFontDatabase>
#include <QDebug>
#include "oinkconfigurator.hpp"

OinkConfigurator::OinkConfigurator(QWidget *parent) :
    QDialog(parent), styleSheet(""),
    projectPath(PROJECT_PATH), buildPath(BUILD_PATH)
{
    //--load style sheet
    QString path = ":/src/resource/stylesheet.css"; //set this as a string... or path won't work!
    QFile file(path);
    file.open(QFile::ReadOnly);
    styleSheet = QLatin1String(file.readAll());

    //--set up cipher
    std::string d = "03/12/14";
    date.setKey(d);
}

QString OinkConfigurator::getBuildPath() const
{
    return buildPath;
}

QString OinkConfigurator::getProjectPath() const
{
    return projectPath;
}

OinkConfigurator::~OinkConfigurator()
{

}

void OinkConfigurator::loadFonts()
{
    QStringList list;
    list << "Rumpelstiltskin.ttf" << "Ubuntu-C.ttf" << "Ubuntu-L.ttf" << "Ubuntu-M.ttf";
    int fontID(-1);
    bool fontWarningShown(false);
    for (QStringList::const_iterator constIterator = list.constBegin(); constIterator != list.constEnd(); ++constIterator)
    {
        QFile res(":/src/resource/fonts/" + *constIterator);
        if (res.open(QIODevice::ReadOnly) == false)
        {
            if (fontWarningShown == false)
            {
                qDebug() << "Could not read font";
                fontWarningShown = true;
            }
        }
        else
        {
            fontID = QFontDatabase::addApplicationFontFromData(res.readAll());
            if (fontID == -1 && fontWarningShown == false)
            {
                qDebug() << "Could not add font";
                fontWarningShown = true;
            }
        }
    }
}

/**
 * @return the Rumpelstiltskin font
 */
QFont OinkConfigurator::getFontRumpel()
{
    QFont font("Rumpelstiltskin");
    return font;
}

/**
 * @return the Ubuntu Light font
 */
QFont OinkConfigurator::getUbuntuLight()
{
    QFont font("Ubuntu Light");
    return font;
}

/**
 * @return the Ubuntu Regular font
 */
QFont OinkConfigurator::getUbuntuRegular()
{
    QFont font("Ubuntu");
    return font;
}

/**
 * @return the Rumpelstiltskin font
 */
QFont OinkConfigurator::getUbuntuCondensed()
{
    QFont font("Ubuntu Condensed");
    return font;
}

/**
 * @return the CSS style sheet
 */
QString OinkConfigurator::getStyleSheet()
{
    return styleSheet;
}

QString OinkConfigurator::encrypt(const QString &text)
{
    std::string cpp_str = text.toStdString();
    QString ret = QString::fromStdString(date.encrypt(cpp_str));
    return ret;
}

QString OinkConfigurator::decrypt(const QString &text)
{
    std::string cpp_str = text.toStdString();
    QString ret = QString::fromStdString(date.decrypt(cpp_str));
    return ret;
}

void OinkConfigurator::mailSent(const QString &status)
{
    if (status == "Message sent")
    {
        qDebug() << "Email was successfully sent!";
    }
    else
    {
        qDebug() << "Could not send email.";
    }

}

void OinkConfigurator::sendEmail(const QString &emailTo, const QString &subject, const QString &message)
{
    //create a new SMTP object with (username, password, mailserver, timeout) <-- we will use this acc to send emails
    Smtp *smtp = new Smtp("OinkChat.ReplyToUser", "CIS330OINK", "smtp.gmail.com", 465);
    connect(smtp, SIGNAL(status(QString)), this, SLOT(mailSent(QString)));

    //first param is the sender's email addr
    smtp->sendMail("OinkChat.ReplyToUser@gmail.com", emailTo , subject, message);

    //cannot use delete or use this as a private data member then delete, causes errors.
}

bool OinkConfigurator::isValidEmail(const QString &email)
{
    if (email.length() == 0)
    {
        return false;
    }

    QRegExp rx("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    rx.setCaseSensitivity(Qt::CaseInsensitive);
    rx.setPatternSyntax(QRegExp::RegExp);

    return rx.exactMatch(email);
}

bool OinkConfigurator::isValidUsername(const QString &username)
{
    if (username.length() == 0)
    {
        return false;
    }

    QRegExp rx("^[A-Za-z][A-Za-z0-9]{5,15}$");
    rx.setPatternSyntax(QRegExp::RegExp);
    return rx.exactMatch(username);
}

/**
 * Checks the password string to see if it's correctly formed
 * @return true if valid, false if not
 */
bool OinkConfigurator::isValidPassword(const QString &password)
{
    return password.length() >= 8;
}
