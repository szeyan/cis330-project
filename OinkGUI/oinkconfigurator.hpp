#ifndef OINKCONFIGURATOR_HPP
#define OINKCONFIGURATOR_HPP

#include <QDialog>
#include <QString>
#include <QFont>
#include <QtGui>
#include "Cipher/date.hpp"
#include "SMTP/smtp.h"

namespace Ui
{
class OinkConfigurator;
}

/**
 * This class is used for the following:
 *      - setting the stylesheets and fonts for all classes
 *      - encrypting and decrypting strings
 *      - email validation
 *      - sending emails using SMTP
 *
 * @author SzeYan Li for styles/fonts, encapsulation of encryption/decryption, and extended validation
 * @author Ghufran Babaeer for email validation
 */
class OinkConfigurator : public QDialog
{
        Q_OBJECT

    public:
        /**
         * Constructor
         */
        explicit OinkConfigurator(QWidget *parent = 0);

        /**
         * Destructor
         */
        ~OinkConfigurator();


        /**
         * Loads the fonts into the application
         * reference: http://www.qtforum.org/article/25367/embedding-custom-fonts.html
         */
        void loadFonts();

        /**
         * @return the Rumpelstiltskin font
         */
        QFont getFontRumpel();

        /**
         * @return the Ubuntu Light font
         */
        QFont getUbuntuLight();

        /**
         * @return the Ubuntu Regular font
         */
        QFont getUbuntuRegular();

        /**
         * @return the Rumpelstiltskin font
         */
        QFont getUbuntuCondensed();

        /**
         * @return the CSS style sheet
         */
        QString getStyleSheet();

        /**
         * Encrypts a given text
         * @return the encrypted text
         */
        QString encrypt(const QString &text);

        /**
         * Decrypts a given text
         * @return the decrypted text
         */
        QString decrypt(const QString &text);

        /**
         * Sends an email
         * @param emailTo email address to email to
         * @param subject email message subject
         * @param message contents that will be sent
         */
        void sendEmail(const QString &emailTo, const QString &subject, const QString &message);

        /**
         * Checks the email string to see if it's correctly formed
         * @return true if valid, false if not
         */
        bool isValidEmail(const QString &email);

        /**
         * Checks the username string to see if it's correctly formed
         *      - must start with a letter
         *      - must be in the range of 5-15 characters long
         *      - alphanumeric characters only
         *      - ^[A-Za-z][A-Za-z0-9]{5,15}$
         * @return true if valid, false if not
         */
        bool isValidUsername(const QString &username);

        /**
         * Checks the password string to see if it's correctly formed
         *      - must be >=8 characters long
         * @return true if valid, false if not
         */
        bool isValidPassword(const QString &password);

        /**
         * @return the path to the directory that the project was built in
         */
        QString getBuildPath() const;

        /**
         * @return the path to the direcotry that the project exists in
         */
        QString getProjectPath()const;

    private slots:
        /**
         * Outputs the status if an email of the user account details was sent
         * @param status whether the send succeeded
         */
        void mailSent(const QString &status);

    private:
        QString styleSheet; ///< A CSS stylesheet
        DateCipher date; ///< DateCipher class object
        QString projectPath; ///< path of the project
        QString buildPath; ///< path to where the client project is built
};

#endif // OINKCONFIGURATOR_HPP
