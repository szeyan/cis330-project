#include <QTimer>
#include "register.hpp"
#include "ui_register.h"
#include "about.hpp"

Register::Register(QWidget *parent) :
    QDialog(parent), ui(new Ui::Register),
    framelessHelper(NULL), effect(NULL),
    targetIpAddress("127.0.0.1"), targetPort(252525)
{
    //--load up GUI elements
    initGUI();

    //--set up the socket
    authenSocket = new QTcpSocket(this);
    connect(authenSocket, SIGNAL(readyRead()), this, SLOT(verification()));
}

void Register::initGUI()
{
    //--we want no frames and we want our own custom shadows
    Qt::WindowFlags flags = 0;
    flags |= Qt::FramelessWindowHint;
    flags |= Qt::NoDropShadowWindowHint;
    this->setWindowFlags(flags);

    //--add a context menu
    QAction *quitAction = new QAction(tr("E&xit"), this);
    quitAction->setObjectName("rightcontextmenu");
    quitAction->setShortcut(tr("Ctrl+Q"));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
    addAction(quitAction);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    //--set up the ui (must be set up before setting the font for widgets)
    ui->setupUi(this);
    this->setWindowTitle("");

    //--set style sheet
    this->setStyleSheet(oinker.getStyleSheet());

    //--set the fonts for the labels/widgets (because macs can't load custom fonts, we'll just load images
#if defined(Q_OS_WIN) || defined(Q_OS_UNIX) //tested to be working on windows and unix at least
    oinker.loadFonts();
    ui->email_label->setFont(oinker.getUbuntuCondensed());
    ui->username_label->setFont(oinker.getUbuntuCondensed());
    ui->pwd_label->setFont(oinker.getUbuntuCondensed());
    ui->pwd_again_label->setFont(oinker.getUbuntuCondensed());
    ui->register_title->setFont(oinker.getUbuntuLight());
    ui->credit_label->setFont(oinker.getUbuntuRegular());
    ui->submit_button->setFont(oinker.getUbuntuCondensed());
    ui->cancel_button->setFont(oinker.getUbuntuCondensed());
#endif

#if defined(Q_OS_MAC) || (!defined(Q_OS_WIN) && !defined(Q_OS_UNIX))
    ui->username_label->setText("");
    ui->username_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/username.png) no-repeat left center;");
    ui->pwd_label->setText("");
    ui->pwd_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/password.png) no-repeat left center;");
    ui->pwd_again_label->setText("");
    ui->pwd_again_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/passwordagain.png) no-repeat left center;");
    ui->email_label->setText("");
    ui->email_label->setStyleSheet("background: #6F2E50 url(:/src/resource/macsupport/email.png) no-repeat left center;");
    ui->submit_button->setText("");
    ui->submit_button->setStyleSheet("background: #FDB71F url(:/src/resource/macsupport/submit.png) no-repeat center;");
    ui->cancel_button->setText("");
    ui->cancel_button->setStyleSheet("background: #FDB71F url(:/src/resource/macsupport/cancel.png) no-repeat center;");
#endif

    //--add a dropshadow to our form
    this->setAttribute(Qt::WA_TranslucentBackground); //hide main window
    effect = new QGraphicsDropShadowEffect();
    effect->setBlurRadius(15);
    effect->setColor(QColor("#000000"));
    effect->setOffset(QPoint(effect->xOffset() - 2, effect->yOffset() - 2));
    ui->shadower->setGraphicsEffect(effect);
    ui->shadower->setStyleSheet("#shadower { background-color: #6F2E50; color: #F5F5F5;}");

    //--set up how we want our frameless window
    framelessHelper = new NcFramelessHelper;
    framelessHelper->activateOn(this);
    framelessHelper->setWidgetMovable(true);
    framelessHelper->setWidgetResizable(true);

    //--we'll show this response message upon user interaction
    ui->responseLabel->setStyleSheet("#responseLabel { color: #6F2E50; font-size:10pt; }");

}

Register::~Register()
{
    delete effect;
    delete framelessHelper;
    delete ui;
    delete authenSocket;
}

void Register::setIPandPort(const QString &ip, int port)
{
    targetIpAddress = ip;
    targetPort = port;
}

bool Register::on_submit_button_clicked()
{
    //--reset the response label. responseLabel is used for displaying a message if user failed to login.
    ui->responseLabel->setStyleSheet("#responseLabel { color: #6F2E50; font-size:10pt; }");

    //--get our variables
    QString username = ui->username_edit->text();
    QString password = ui->pwd_edit->text();
    QString passwordAgain = ui->pwd_again_edit->text();
    QString email =  ui->email_edit->text();

    //--validate the entered fields
    if (!validateAll(username, password, passwordAgain, email))
    {
        shakeGUI();
        return false;
    }

    //--set the oink object
    OinkObject o;
    o.setFlag("register");
    o.setUser(UserObject(oinker.encrypt(username), oinker.encrypt(password), oinker.encrypt(email)));

    //--read oink object into datastream
    QByteArray block; // temporarily stores the data we are writing
    QDataStream out(&block, QIODevice::WriteOnly); // writes in data
    out.setVersion(QDataStream::Qt_5_2);  // version needs to match OinkClient's
    out << o;

    //--connect and send
    authenSocket->connectToHost(targetIpAddress, targetPort);

    //--check for timeout
    if (!authenSocket->waitForConnected(2000))
    {
        qDebug() << "Error: " << authenSocket->errorString();
        ui->responseLabel->setText("Could not connect to server.");
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        shakeGUI();
        authenSocket->close();
        return false;
    }

    authenSocket->write(block);

    return true;

}

void Register::verification()
{
    //--retrieve the oink object
    QDataStream in(authenSocket);
    OinkObject oinkObject;
    in >> oinkObject;
    QString flag = oinkObject.getFlag();

    //--close the socket no matter what
    authenSocket->close();

    //--process flag
    if (flag == "register")
    {
        if (oinkObject.isAck())
        {
            //--reponse message
            ui->responseLabel->setText("Registration successful!  An email was also sent with your new account details.");
            ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");

            //--send an email to the user with their account details as well
            QString username = oinkObject.getUser().getName();
            QString password = oinkObject.getUser().getPassword();
            QString email =  oinkObject.getUser().getEmail();
            QString msg = "Your username is " + username + "\nYour password is " + password;
            QString header = "Welcome to OinkChat!";
            oinker.sendEmail(email, header, msg);
        }
        else
        {
            //--display error message
            ui->responseLabel->setText("Username or email is already registered.");
            ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
            shakeGUI();
        }

    }
}

bool Register::validateAll(QString username, QString password, QString passwordAgain, QString email)
{
    //--check for valid user name
    if (!oinker.isValidUsername(username))
    {
        //--display error message
        if (username.length() < 5 || username.length() > 15)
        {
            ui->responseLabel->setText("Username must be 5 to 15 characters long.");
        }
        else if (!username.at(0).isLetter())
        {
            ui->responseLabel->setText("Username must begin with a letter.");
        }
        else
        {
            ui->responseLabel->setText("Invalid username format.");
        }
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        return false;
    }

    //--check for valid password
    if (!oinker.isValidPassword(password))
    {
        //--display error message
        ui->responseLabel->setText("Password must be 8 or more characters.");
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        return false;
    }

    //--check that both passwords match
    if (password.compare(passwordAgain) != 0)
    {
        //--display error message
        ui->responseLabel->setText("Passwords do not match.");
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        return false;
    }


    //--check if the email address is valid
    if (!oinker.isValidEmail(email))
    {
        //--display error message
        if (email.isEmpty())
        {
            ui->responseLabel->setText("Please enter your email.");
        }
        else
        {
            ui->responseLabel->setText("Invalid email format.");
        }
        ui->responseLabel->setStyleSheet("#responseLabel { color: #FDB71F; font-size:10pt; }");
        return false;
    }

    return true;
}

void Register::on_cancel_button_clicked()
{
    this->close();
}

void Register::on_credit_label_clicked()
{
    //--show the about box
    About about;
    about.setModal(true);
    about.exec();
}

void Register::shakeGUI()
{

    //we only want this to shake on mac or windows, since it doesn't work with unix
#if defined(Q_OS_WIN) || defined(Q_OS_MAC)

    static int numTimesCalled = 0;
    static bool shakeSwitch = false;
    QPoint offset(10, 0);

    //--incre times called
    numTimesCalled++;

    //--max calls to self is 9 times
    if (numTimesCalled == 9)
    {
        numTimesCalled = 0;
        return;
    }

    //--move it either left or right depending on the shake switch
    move(((shakeSwitch) ? pos() + offset : pos() - offset));

    //--alternate the direction
    if (shakeSwitch)
    {
        shakeSwitch = false;
    }
    else
    {
        shakeSwitch = true;
    }

    //--shake for 40 msec
    QTimer::singleShot(40, this, SLOT(shakeGUI()));

#endif
}
