#ifndef REGISTER_H
#define REGISTER_H

#include <QtCore>
#include <QtGui>
#include <QDialog>
#include <QGraphicsDropShadowEffect>
#include "NcFramelessHelper/src/NcFramelessHelper.h"
#include "oinkconfigurator.hpp"
#include "Protocol/oinkobject.hpp"

namespace Ui
{
class Register;
}

/**
 * The register GUI is responsible for user signup
 *
 * @author SzeYan Li for GUI elements
 * @author Hanxiao Zhang for networking
 */
class Register : public QDialog
{
        Q_OBJECT

    public:
        /**
         * Constructor
         */
        explicit Register(QWidget *parent = 0);
        /**
         * Destructor
         */
        ~Register();

        /**
         * Sets the ip and port that the chatroom should connect to
         * @param ip the IP address to connect to
         * @param port the port to connect to
         */
        void setIPandPort(const QString &ip, int port);

    private slots:
        /**
         * Closes this window and goes back to the login screen
         */
        void on_cancel_button_clicked();

        /**
         * submits the email inputted to the server to retrieve user info
         * @return true, if can register.  false if not.
         */
        bool on_submit_button_clicked();

        /**
         * Opens up the about gui
         */
        void on_credit_label_clicked();

        /**
         * Shakes the GUI (only on Windows and Mac)
         * reference: //http://stackoverflow.com/questions/10677324/possible-to-make-qdialog-shake-in-osx
         */
        void shakeGUI();

        /**
         * Receive a feedback from the server to see if this user exists
         */
        void verification();

    private:
        //GUI variables
        Ui::Register *ui; ///< The gui connected to this class
        OinkConfigurator oinker; ///< a configurator for fonts/styles/encrypt/decrypt
        NcFramelessHelper *framelessHelper; ///< used to create frameless gui window
        QGraphicsDropShadowEffect *effect; ///< used to create a dropshadow effect for the gui

        //Networking variables
        QString targetIpAddress; ///< the ip address to connect to
        QTcpSocket *authenSocket; ///< the socket connected to the server
        int targetPort; ///< the port to connect to

        /**
         * Helper function for initializing the gui elements
         */
        void initGUI();

        /**
         * Helper function for validating the username, password, and email
         * @param username this user's desired username
         * @param password this user's desired password
         * @param passwordAgain this user's re-entered password
         * @param email this user's desired email
         * @return true if valid, false if invalid
         */
        bool validateAll(QString username, QString password, QString passwordAgain, QString email);
};

#endif // REGISTER_H
