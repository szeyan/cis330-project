#include <string>
#include <cctype>
#include <iostream>
#include "date.hpp"

DateCipher::DateCipher()
{
    for (int i = 0; i < 6; i++) key[i] = 0;
}


DateCipher::DateCipher(std::string date)
{
    if (isValidDate(date))
    {
        for (int i = 0, j = 0; i < 8; i++)
        {
            if (i == 2 || i == 5) continue;
            key[j] = date[i] - '0';
            j++;
        }
    }
    else
    {
        std::cout << "Invalid input. Default date 00/00/00 will be used" << std::endl;
        for (int i = 0; i < 6; i++) key[i] = 0;
    }
}

DateCipher::~DateCipher()
{

}

std::string DateCipher::encrypt(std::string &inputText)
{
    std::string text = inputText;
    int ptr = 0;
    for (int i = 0; i < (int) text.length(); i++)
    {
        if (std::isupper(text[i]))
        {
            text[i] = text[i] + key[ptr];
            if ((int)text[i] > 90) text[i] -= 26;
            // iterate through the key. go back to the first element if it reaches the end
            ptr++;
            if (ptr == 6) ptr = 0;
        }
        else if (std::islower(text[i]))
        {
            int result = text[i] + key[ptr]; // not making result a char type to avoid having a char whose value may be over 127
            if (result > 122) result -= 26;
            text[i] = (char) result;
            // iterate through the key. go back to the first element if it reaches the end
            ptr++;
            if (ptr == 6) ptr = 0;
        }
        else if (std::isdigit(text[i]))
        {
            int result = text[i] + key[ptr];
            if (result > 57) result -= 10;
            text[i] = (char) result;
            // iterate through the key. go back to the first element if it reaches the end
            ptr++;
            if (ptr == 6) ptr = 0;
        }

        // don't do anything with any other char such as a space or a punctuation

    }
    return text;

}

std::string DateCipher::decrypt(std::string &inputText)
{
    std::string text = inputText;
    int ptr = 0;
    for (int i = 0; i < (int) text.length(); i++)
    {
        if (std::isupper(text[i]))
        {
            text[i] = text[i] - key[ptr];
            if ((int)text[i] < 65) text[i] += 26;
            // iterate through the key. go back to the first element if it reaches the end
            ptr++;
            if (ptr == 6) ptr = 0;
        }
        else if (std::islower(text[i]))
        {
            text[i] = text[i] - key[ptr];
            if ((int)text[i] < 97) text[i] += 26;
            // iterate through the key. go back to the first element if it reaches the end
            ptr++;
            if (ptr == 6) ptr = 0;
        }
        else if (std::isdigit(text[i]))
        {
            text[i] = text[i] - key[ptr];
            if ((int)text[i] < 48) text[i] += 10;
            // iterate through the key. go back to the first element if it reaches the end
            ptr++;
            if (ptr == 6) ptr = 0;
        }

        // don't do anything with any other char such as a space or a punctuation

    }

    return text;
}

bool DateCipher::isValidDate(std::string date)
{
    if (!(date.length() == 8 && date[2] == '/' && date[5] == '/'))
    {
        return false;
    }
    for (int i = 0; i < 8; i++)
    {
        if (i == 2 || i == 5) continue;
        if (!(isdigit(date[i]))) return false;
    }
    return true;
}

bool DateCipher::setKey(std::string date)
{
    if (isValidDate(date))
    {
        for (int i = 0, j = 0; i < 8; i++)
        {
            if (i == 2 || i == 5) continue;
            key[j] = date[i] - '0';
            j++;
        }
        return true;
    }
    return false;
}
