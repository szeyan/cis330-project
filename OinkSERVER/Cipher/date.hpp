#ifndef DATECIPHER_HPP_
#define DATECIPHER_HPP_

/**
 * A Cipher class which uses date shifting to encrypt and decrypt numbers and strings
 *
 * @author Hanxiao Zhang (and edited by SzeYan Li for setKey, default constructor, dec/enc of numbers)
 */
class DateCipher
{

    public:
        /**
         * Constructor
         */
        DateCipher();

        /**
         * Constructor
         * @param date the key to shift by
         */
        DateCipher(std::string date);

        /**
         * Destructor
         */
        virtual ~DateCipher();

        /**
         * Encrypts a given string
         * @param inputText string to encrypt
         * @return the encrypted string
         */
        virtual std::string encrypt(std::string &inputText);

        /**
         * Decrypts a given string
         * @param inputText string to decrypt
         * @return the decrypted string
         */
        virtual std::string decrypt(std::string &inputText);

        /**
         * Checks if the key given is valid
         *
         * @return true if input is a valid date
         */
        virtual bool isValidDate(std::string date);

        /**
         * Sets the key for the cipher to shift by
         *
         * @return true if valid key was set
         */
        bool setKey(std::string date);

    private:
        int key[6]; ///< The cipher key
};


#endif
