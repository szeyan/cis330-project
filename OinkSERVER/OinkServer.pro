#-------------------------------------------------
#
# Project created by QtCreator 2014-02-18T15:13:00
#
#-------------------------------------------------

QT       += core
QT       += network
QT       += sql
QT       -= gui

TARGET = OinkServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    oinkserver.cpp \
    Protocol/messageobject.cpp \
    Protocol/oinkobject.cpp \
    Protocol/userobject.cpp \
    oinkdb.cpp \
    Cipher/date.cpp


HEADERS += \
    oinkserver.hpp \
    Protocol/messageobject.hpp \
    Protocol/oinkobject.hpp \
    Protocol/userobject.hpp \
    oinkdb.hpp \
    Cipher/date.hpp

DEFINES += PROJECT_PATH=\"\\\"$$PWD\\\"\"
DEFINES += BUILD_PATH=\"\\\"$$OUT_PWD\\\"\"
