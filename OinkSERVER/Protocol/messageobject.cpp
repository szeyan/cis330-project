#include "messageobject.hpp"

MessageObject::MessageObject() : message(""), sender(""), receiver("") {}

MessageObject::MessageObject(QString message, QString sender, QString receiver) :
    message(message), sender(sender), receiver(receiver) {}

MessageObject::~MessageObject() {}

QString MessageObject::getMessage() const
{
    return this->message;
}

QString MessageObject::getSender() const
{
    return this->sender;
}

QString MessageObject::getReceiver() const
{
    return this->receiver;
}

void MessageObject::setMessage(QString message)
{
    this->message = message;
}

void MessageObject::setSender(QString sender)
{
    this->sender = sender;
}

void MessageObject::setReceiver(QString receiver)
{
    this->receiver = receiver;
}

QDataStream &operator<<(QDataStream &out, const MessageObject &messageObject)
{
    out << messageObject.getMessage()
        << messageObject.getSender() << messageObject.getReceiver();
    return out;
}

QDataStream &operator>>(QDataStream &in, MessageObject &messageObject)
{
    QString message;
    QString sender;
    QString receiver;

    in >> message >> sender >> receiver;
    messageObject = MessageObject(message, sender, receiver);
    return in;
}
