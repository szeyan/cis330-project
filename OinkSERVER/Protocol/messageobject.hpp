#ifndef MESSAGEOBJECT_HPP
#define MESSAGEOBJECT_HPP

#include <QString>
#include <QDataStream>

/**
 * This class holds a "message" to be transported over the network.  Contains the usernames of the receiver and the sender
 *
 * @author Hanxiao Xi, Xingwei Cao
 */
class MessageObject
{

    public:
        /**
         * Constructor: we need to explicitly define the default constructor for Qt serialization
         */
        MessageObject();
        /**
         * Constructor
         * @param message to be transported
         * @param sender of the message
         * @param receiver of the message
         */
        MessageObject(QString message, QString sender, QString receiver);
        /**
         * Destructor
         */
        ~MessageObject();

        /**
         * @return the message
         */
        QString getMessage() const;

        /**
         * @return the sender
         */
        QString getSender() const;

        /**
         * @return the receiver
         */
        QString getReceiver() const;

        /**
         * Sets the message
         * @param message to be set
         */
        void setMessage(QString message);

        /**
         * Sets the sender
         * @param sender of the message
         */
        void setSender(QString sender);

        /**
         * Sets the receiver
         * @param receiver of the message
         */
        void setReceiver(QString receiver);

        /**
         * Overloading operator: out
         */
        friend QDataStream &operator<<(QDataStream &out, const MessageObject &messageObject);

        /**
         * Overloading operator: in
         */
        friend QDataStream &operator>>(QDataStream &in, MessageObject &messageObject);

    private:
        QString message; ///< the message to be transported
        QString sender; ///< the sender of the message
        QString receiver; ///< the recipient of the message
} ;

#endif
