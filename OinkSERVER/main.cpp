#include <QCoreApplication>
#include "oinkserver.hpp"
#include "oinkdb.hpp"

int main(int argc, char *argv[])
{
    qDebug() << "Compiled with Qt Version: " << QT_VERSION_STR;
    QCoreApplication a(argc, argv);

    QString projectPath(PROJECT_PATH);
    QString buildPath(BUILD_PATH);
    qDebug() << "Project path: " << projectPath;
    qDebug() << "Build path: " << buildPath;

    OinkServer oinkServer;
    return a.exec();
}
