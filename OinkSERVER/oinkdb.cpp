#include "oinkdb.hpp"

OinkDB::OinkDB(const QString &dirPath): dbPath(""), absoluteDIR("")
{
    //from SzeYan Li: using QCoreApplication::applicationDirPath() for absolute path would work
    //only if you COPIED (not compiled) the database.sqlite file into the debug/release folder.
    //can't use QtResource files with .sqlite files, so must hardcode path

    absoluteDIR = dirPath;
    dbPath = absoluteDIR + "/database.sqlite";
    qDebug() << "Database path: " << dbPath;

    //--open database
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dbPath);
    if (!db.open())
    {
        qDebug() << "cannot open database!";
    }
}

OinkDB::~OinkDB()
{
    //--close and remove the database
    db.close();
    QSqlDatabase::removeDatabase("QSQLITE");
}

QString OinkDB::getAbsoluteDIR() const
{
    return absoluteDIR;
}

bool OinkDB::registerUser(const QString &username, const QString &password, const QString &email)
{
    //--check username (case sensitivity ignored)
    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE USERNAME =\'" + username + "\' COLLATE NOCASE");
    if (!q.exec())
    {
        qDebug() << "error executing database: " << q.lastError().text();
    }

    //--search through returned query to see if the username exists
    while (q.next())
    {
        return false; //username exists
    }

    //--check email (case sensitivity ignored)
    q.prepare("SELECT * FROM users WHERE EMAIL =\'" + email + "\' COLLATE NOCASE");
    if (!q.exec())
    {
        qDebug() << "error executing database: " << q.lastError().text();
    }
    //--search through returned query to see if email exists
    while (q.next())
    {
        return false; //email exists
    }

    //--all is okay, so add a new user
    q.prepare("INSERT INTO users (USERNAME, EMAIL, PASSWORD) VALUES (?,?,?)");
    q.addBindValue(username);
    q.addBindValue(email);
    q.addBindValue(password);
    if (!q.exec())
    {
        qDebug() << "error executing database: " << q.lastError().text();
    }

    //--query done. commit database and return
    db.commit();
    return true;
}

bool OinkDB::loginUser(const QString &username, const QString &password)
{
    //--query for username and password (case sensitive)
    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE USERNAME = ? AND PASSWORD = ?");
    q.addBindValue(username);
    q.addBindValue(password);
    if (!q.exec())
    {
        qDebug() << "error executing database: " << q.lastError().text();
    }

    //--search through query results
    bool exists = false;
    while (q.next())
    {
        exists = true;
    }

    return exists;
}

QString OinkDB::getUsername(const QString &email)
{
    //--set up query
    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE EMAIL =\'" + email + "\' COLLATE NOCASE");
    if (!q.exec())
    {
        qDebug() << "error executing database: " << q.lastError().text();
    }

    QString username = "";

    //--search through emails and get username if it exists
    while (q.next())
    {
        username = q.value("USERNAME").toString();
    }

    return username;
}

QString OinkDB::getPassword(const QString &username)
{
    //--set up query
    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE USERNAME = ?");
    q.addBindValue(username);
    if (!q.exec())
    {
        qDebug() << "error executing database: " << q.lastError().text();
    }

    QString password = "";

    //--search through usernames and get password if it exists
    while (q.next())
    {
        password = q.value("PASSWORD").toString();
    }

    return password;
}
