#ifndef OINKDB_HPP
#define OINKDB_HPP

#include <QDebug>
#include <QString>
#include <QtSql>
#include <QSqlQuery>

/**
 * This class is used to encapsulate the SQL database
 *
 * @author Ghufran Babaeer for base code
 * @author SzeYan Li for comments, functionality, and generality (restructuring)
 */

class OinkDB
{
    public:
        /**
         * Constructor.  Takes in the absolute path to the directory of the database
         * @param path is the path to the directory of the database
         */
        OinkDB(const QString &dirPath);

        /**
         * Destructor
         */
        ~OinkDB();

        /**
         * @return the absolute path to the directory of the database
         */
        QString getAbsoluteDIR() const;

        /**
         * check whether the user name and email are not exists.  Adds user to the database if the relation doesn't exist
         * The DB disregards case-sensitivity when looking for username and email
         *
         * @param username the desired username
         * @param password the desired password
         * @param email the desired email
         * @return true if user can register, otherwise return false.
         */
        bool registerUser(const QString &username, const QString &password, const QString &email);

        /**
         * check whether the username and password exists in the database for the login function
         * The DB makes sure case-sensitivity when looking for username
         *
         * @param username the user's username
         * @param password the user's password
         * @return true if user can login, otherwise return false.
         */
        bool loginUser(const QString &username, const QString &password);

        /**
         * Looks for a username by email
         * The DB disregards case-sensitivity when looking for email
         *
         * @param email the email belonging to the username
         * @return the username if there exists a user with the email, empty string if not
         */
        QString getUsername(const QString &email);

        /**
         * Looks for a password by username
         * The DB makes sure case-sensitivity when looking for username
         *
         * @param username the username belonging to the user
         * @return the password if the user exists in the database, empty string if not
         */
        QString getPassword(const QString &username);

    private:
        QString dbPath; ///< database path
        QString absoluteDIR; ///< application path
        QSqlDatabase db; ///< the SQL database
};

#endif // OINKDB_HPP
