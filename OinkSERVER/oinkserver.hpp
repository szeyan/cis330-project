#ifndef OINKSERVER_H
#define OINKSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QVector>
#include <QSignalMapper>
#include <QDataStream>
#include <QDebug>
#include <QFile>
#include <QString>
#include <QtCore>
#include "Protocol/oinkobject.hpp"
#include "Protocol/userobject.hpp"
#include "Protocol/messageobject.hpp"
#include "oinkdb.hpp"
#include "Cipher/date.hpp"

/**
 * A server that processes OinkObjects and interacts with gui.
 *
 * @author Hanxiao Zhang (send, login, register, forgotAcc flags), SzeYan Li (update flag)
 */
class OinkServer : public QObject
{
        Q_OBJECT
    public:
        /**
         * Constructor
         */
        explicit OinkServer(QObject *parent = 0);
        /**
         * Destructor
         */
        ~OinkServer();

        /**
         * Encrypts a given text
         * @return the encrypted text
         */
        QString encrypt(const QString &text);

        /**
         * Decrypts a given text
         * @return the decrypted text
         */
        QString decrypt(const QString &text);

    public slots:
        /**
         * connected to QTcpServer's newConnection() signal
         */
        void getNewClient();
        /**
         * connected to QTcpSocket's disconnected() signal
         * @param socket is the tcp socket
         * QObject will be cast to QTcpSocket in the function
         */
        void loseOneClient(QObject *socket);
        /**
         * calls different functions based on flag
         */
        void processSignalBasedOnFlag();
        /**
         * called by processSignalBasedOnFlag()
         * @param oinkObject contains the message sent
         */
        void redirectMessage(OinkObject oinkObject);
        /**
         * called by redirectMessage()
         * @param oinkObject contains the message sent
         */
        void broadCastClientMessage(OinkObject oinkObject);
        /**
         * called by redirectMessage()
         * @param oinkObject contains the message sent
         */
        void privateChatMessage(OinkObject oinkObject);

    private:

        OinkDB *database; ///< A database instance
        QTcpServer *tcpServer; ///< An instance of server
        QHash<QTcpSocket *, QString> users; ///< user list
        DateCipher date; ///< DateCipher class object

        /**
         * Sends a message to all users with the updated list of current users in the chatroom
         * @param msg the message that should be sent to all users
         */
        void sendUserList(QString msg);
};

#endif // OINKSERVER_H
