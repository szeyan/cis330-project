# ################################
# Oink Chat Application 
# CIS330 - C/C++ And Unix
# ################################

== AUTHORS ==
	Ghufran Babaeer
	Hanxiao Zhang
	SzeYan Li
	Xi Zhang
	Xingwei Cao 

== NOTICE == 	
	For documentation regarding OinkSERVER:
		documentation/OinkSERVER/html/index.html

	For documentation regarding OinkGUI:
		documentation/OinkGUI/html/index.html
		
	** the README is contained within index.html as well
	
	To run the Oink application successfully on your machine, you must configure some of the files to fit your system.  
	Installation details can be found in either index.html under the "BUILD/INSTALLATION" and the "USAGE" section.
	
	Before you begin, please also make sure you have installed Qt Libraries version 5.2.1 or greater.  You'll also need a version of qmake that is compatible with Qt5.2.1.
