Team Flying Piglets
	
Note: each team member is responsible for writing in part 1.  Be sure to commit your changes after you're done.  If you need help, please message your fellow teammates on Skype.

1) Short description of each team member's role and contributions.
  
	SzeYan:
		I was in charge of front-end GUI design.  Since I was the project lead, I kind of made myself the software architect as well and helped direct team members in their tasks.  I made all the artwork.  I checked the status of all team members.  A lot of stuff I did on my own accord.
		
		Contributions:
			- all GUIs + CSS 
			- custom window frame with drop shadow
			- "Remember me" function to load a user'a account information
			- user ability to choose custom font/colors
			- clearing the chat history after 10,000 characters of input to prevent app. lag
			- limiting user input to 500 characters to prevent overloading the server
			- preventing users from inputting their own html
			- validation and restrictions for username/password
			- window shaking feature
			- custom images 
			- commenting all header files 
			- "update" flag in OinkServer.cpp
			- in general, just connecting front-end implementation with back-end implementation
			- general testing/debugging
			- designing the CSS for Doxygen generated HTML files
			
	Xingwei:
		I was in charge of presentation, makefile and Doxygen.
		I worked with all the members except database and GUI.
		
		Contributions:
			- worked with Xi on generating Doxygen HTML files
			- userobject, messageobject and oinkobject for Object Oriented Programming design
			- OOP program's makefile and readme
			- enables Client to send information to Server (worked with Hanxiao)
			- bash script
			- makefile using qmake 5.2.1
			- Prezi presentation and time schedule for presentation
			- prototype of OinkClient (worked with Hanxiao)
			- icon for app

	Hanxiao:
		I worked on network server/client connection and serialization, wrote README and made backup demo videos. 

		Contributions: 
			- created OinkServer class and added multiple functions
			- enabled server/client connection (with Xingwei)
			- added send, login, register and forgotAcc flags with Szeyan's help
			- implemented serialization and overloaded istream and ostream operators for userobject, messageobject and oinkobject (with Xi)
			- wrote README for documentation (with Ghufran)
			- made backup demo videos

  	Xi: 
  		I worked as a documenter use doxygen and supply presentation, also an assitant of network and etc.
  		
  		Contributions:
  		  	- research Prezi, goAnimate and quickTime as presentation tool
  		  	- idea of shaking and sound features
  			- implemented serialization and overloaded istream and ostream operators for oinkobject with Hanxiao
  			- worked with Xingwei on generating Doxygen HTML files, proofread all header file comments in doxygen format
  			- worte README file
  			- generate makefile use qmake with Xingwei's help
  			- advertise video for presntation
  			- detected bugs of spanning operating system platform

  
2) Short description of how the final project differs from the original plan

	Our main goal was to have a functional chatroom with a user's list.  Then, we wanted to extend our functionality to include private chat, registration, login, and user account retrieval.  This also meant that we needed a database.  
	
	Overall, our team was very ambitious (or the project lead was very ambitious).  We had delays in our work, but we implemented everything that we had wanted.  
	
	Features include:
		- SQLite database integration
		- registration with validation
		- login (with no double login allowed)
		- chatroom (with list of current users)
		- private chat 
		- user account retrieval by email
		- and much more.
		
	One main thing that differed was that the project lead listed "incremental development with prototyping," yet prototyping never happened.  We ended up implementing most of the networking functionality very late into our project.  
	
	In conclusion, we did everything we said we wanted to do and more.  
	
	by SzeYan Li