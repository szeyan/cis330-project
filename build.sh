#!/bin/bash          
# ################################
# Bash Script
# 
# This file runs qmake on the relevant project files and moves the executables to 
# the root folder.  
# Assumes that user is running a version of qmake that is compatible with Qt5.2.1
# ################################
# 
## Location Variables
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#
## 
cd $DIR
#
## build OinkServer
cd ./OinkSERVER 
qmake OinkServer.pro
make
cd ..
#
## build OinkGUI
cd ./OinkGUI
qmake OinkGUI.pro
make
cd ..
#
## move the executables
sleep 3
mv ./OinkSERVER/OinkServer ./
mv ./OinkGUI/OinkChat ./
#
## DONE