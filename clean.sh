#!/bin/bash          
# ################################
# Bash Script
# 
# Cleans up the files
# ################################
# 
## Location Variables
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#
## 
cd $DIR
#
## clean up executables
rm -f ./OinkServer
rm -f ./OinkChat
## clean up OinkServer
cd ./OinkSERVER
make clean
sleep 3
rm -f Makefile
cd ..
## clean up OinkGUI
cd ./OinkGUI
make clean
sleep 3
rm -f Makefile
#
## DONE