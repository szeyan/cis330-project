var searchData=
[
  ['getabsolutedir',['getAbsoluteDIR',['../class_oink_d_b.html#a74c21a58955ddd830c5907f7178e4464',1,'OinkDB']]],
  ['getemail',['getEmail',['../class_user_object.html#a35822594a44e145d3329234ff1fe7915',1,'UserObject']]],
  ['getflag',['getFlag',['../class_oink_object.html#a5db9881ae42f19870a342e7120f94b05',1,'OinkObject']]],
  ['getmessage',['getMessage',['../class_message_object.html#a65b3879d453792210fffeda687c04eea',1,'MessageObject::getMessage()'],['../class_oink_object.html#a9e4beace9ded5fc6bc9fc2ff9234d59f',1,'OinkObject::getMessage()']]],
  ['getname',['getName',['../class_user_object.html#a09a7108aa6305a620391d292aa14c9bc',1,'UserObject']]],
  ['getnewclient',['getNewClient',['../class_oink_server.html#a8a0f153533d00a81256558997a0ca0cc',1,'OinkServer']]],
  ['getpassword',['getPassword',['../class_oink_d_b.html#a251df2053fbc12ff78123842f2b7917c',1,'OinkDB::getPassword()'],['../class_user_object.html#aada4494742920d15f94f22e607f3325f',1,'UserObject::getPassword()']]],
  ['getreceiver',['getReceiver',['../class_message_object.html#a11d930602701f409e51297be000b7016',1,'MessageObject']]],
  ['getsender',['getSender',['../class_message_object.html#ad805b810d824b94c8f2b3282737ea802',1,'MessageObject']]],
  ['getuser',['getUser',['../class_oink_object.html#abcb1b5855b7f31ead9d7536aa69fe8a0',1,'OinkObject']]],
  ['getuserlist',['getUserList',['../class_oink_object.html#a41c3546f7d0a9bf724091b483d506a92',1,'OinkObject']]],
  ['getusername',['getUsername',['../class_oink_d_b.html#ab2f2743c3a132fc0249d6ed771fd48ae',1,'OinkDB']]]
];
