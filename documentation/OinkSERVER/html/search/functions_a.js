var searchData=
[
  ['senduserlist',['sendUserList',['../class_oink_server.html#a33353634a96aee32f1f564d83288ae6b',1,'OinkServer']]],
  ['setack',['setAck',['../class_oink_object.html#a90a445638491466e42922cad2780002a',1,'OinkObject']]],
  ['setemail',['setEmail',['../class_user_object.html#a996da38f9a5dae4e72e5f73fe4fb6244',1,'UserObject']]],
  ['setflag',['setFlag',['../class_oink_object.html#a0dbe84bee673eaaf271508201822cc12',1,'OinkObject']]],
  ['setkey',['setKey',['../class_date_cipher.html#a592f58aa275e82bfc024d4d948c1f1b5',1,'DateCipher']]],
  ['setmessage',['setMessage',['../class_message_object.html#a8a2a1bb75f87de78203b5d5c6ceba13d',1,'MessageObject::setMessage()'],['../class_oink_object.html#a3562de4efb0e6a15a10fc301081db7a1',1,'OinkObject::setMessage()']]],
  ['setname',['setName',['../class_user_object.html#a4fe26ecd6a06647a08d77dde44591b79',1,'UserObject']]],
  ['setpassword',['setPassword',['../class_user_object.html#a16b609f39ffc3ed80681ffc20801b023',1,'UserObject']]],
  ['setreceiver',['setReceiver',['../class_message_object.html#a042abb169c3b856aac0a87a97416d7e1',1,'MessageObject']]],
  ['setsender',['setSender',['../class_message_object.html#a9fc4e09b32c4531801c066e61ad3f48c',1,'MessageObject']]],
  ['setuser',['setUser',['../class_oink_object.html#a559f4b0db2a3fa87d55b15499f9cba27',1,'OinkObject']]],
  ['setuserlist',['setUserList',['../class_oink_object.html#a0141adea52e497c1f6039855298d0e03',1,'OinkObject']]]
];
