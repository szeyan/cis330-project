#-------------------------------------------------
#
# Project created by QtCreator 2014-03-02T16:40:11
#
#-------------------------------------------------

QT       += core gui widgets network sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = Oink
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    oinkuser.cpp \
    oinkdb.cpp \
    smtp.cpp \
    validation.cpp \
    mainwindow.cpp

HEADERS += \
    oinkuser.h \
    oinkdb.h \
    smtp.h \
    validation.h \
    mainwindow.h

OTHER_FILES += \
    database.sqlite 

FORMS += \
    mainwindow.ui
