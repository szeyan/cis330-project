#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "validation.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget_4->setCurrentWidget(ui->login_4);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loginButton_4_clicked()
{
    userObj.userName = ui->username_lineEdit_4->text();
    userObj.userPassword = ui->password_LineEdit_4->text();
    qDebug()<<"try to login";
    // connect to database:
    bool logged = dbObj.login(userObj);
    if( logged )
    {
        ui->stackedWidget_4->setCurrentWidget(ui->page_6);

    }else{
        QMessageBox msgBox;
        msgBox.setText("wrong username or password!");
        msgBox.exec();
    }


}

void MainWindow::on_signupButton_7_clicked()
{
    ui->username_label_4->setVisible(false);
    ui->email_label_4->setVisible(false);
    ui->pass_label_4->setVisible(false);
    ui->confirm_label_4->setVisible(false);
    ui->username_exist_label_4->setVisible(false);
    ui->stackedWidget_4->setCurrentWidget(ui->signup_4);

}

void MainWindow::on_forgetButton_4_clicked()
{
    ui->stackedWidget_4->setCurrentWidget(ui->forgetPage_4);

}

void MainWindow::on_resetButton_4_clicked()
{
    QString email = ui->resetEmailLineEdit_5->text();
    bool found = dbObj.forgetPassword(email);
    if( found )
    {

        QMessageBox msgBox;
        msgBox.setText("An email was sent to your email address.");
        msgBox.exec();

    }else{
           QMessageBox msgBox;
           msgBox.setText("The email address you submitted was not found in the database!.");
           msgBox.exec();
    }
    ui->stackedWidget_4->setCurrentWidget(ui->login_4);
}

void MainWindow::on_cancelButton_5_clicked()
{
    ui->stackedWidget_4->setCurrentWidget(ui->login_4);

}

void MainWindow::on_signupButton_8_clicked()
{
    ui->username_label_4->setVisible(false);
    ui->username_exist_label_4->setVisible(false);
    //ui->email_exist_label_4->setVisible(false);
    ui->email_label_4->setVisible(false);
    ui->pass_label_4->setVisible(false);
    ui->confirm_label_4->setVisible(false);
    QString name = ui->nameLineEdit_5->text();
    QString email = ui->EmailLineEdit_5->text();
    QString pass = ui->PasswordLineEdit_5->text();
    QString passConfirm = ui->confirmPasswordLineEdit_5->text();

    // Check with regex:
    Validation validation;
    QString msg=validation.validData(name,email,pass,passConfirm);
    if(msg==""){//send data to server
        userObj.userEmail = email;
        userObj.userName  = name;
        userObj.userPassword = pass;
        bool registered = dbObj.registerUser(userObj);
        if(!registered)
            ui->username_exist_label_4->setVisible(true);

        else{
            QMessageBox msgBox;
            msgBox.setText("user created successfully ! ");
            msgBox.exec();
            ui->stackedWidget_4->setCurrentWidget(ui->login_4);
        }
        return;



    }
    if(msg.contains("username;")){
        ui->username_label_4->setVisible(true);
    }
    if(msg.contains("email;")){
        ui->email_label_4->setVisible(true);
    }
    if(msg.contains("password;")){
        ui->pass_label_4->setVisible(true);
    }
    if(msg.contains("confirmPassword;")){
        ui->confirm_label_4->setVisible(true);
    }

}
