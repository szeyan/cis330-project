#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "oinkuser.h"
#include "oinkdb.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_loginButton_4_clicked();




    void on_signupButton_7_clicked();

    void on_forgetButton_4_clicked();

    void on_resetButton_4_clicked();

    void on_cancelButton_5_clicked();

    void on_signupButton_8_clicked();

private:
    Ui::MainWindow *ui;
    OinkUser userObj;
    OinkDB dbObj;

};

#endif // MAINWINDOW_H
