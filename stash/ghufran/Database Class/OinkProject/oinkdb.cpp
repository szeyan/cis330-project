#include "oinkdb.h"
#include <QtSql>
#include <QSqlQuery>

OinkDB::OinkDB(QObject *parent):
    QObject(parent)
{

}

bool OinkDB::registerUser(OinkUser userObj){

    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/ghofranbabeir/Desktop/OinkProject/database.sqlite");
    if(!db.open()){
         qDebug() <<"cannot open database!";
    }

    // check if username exists :

    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE USERNAME = ?");
    q.addBindValue(userObj.userName);

    if (!q.exec())
    {
         qDebug() << q.lastError().text();
    }

    bool usernameFound = false;

    while (q.next())
    {
        usernameFound = true;
        qDebug() << "username exists";
        break;
    }


    if(usernameFound){ // username exists:
        db.close();
        return false;
    }
    q.prepare("SELECT * FROM users WHERE EMAIL = ?");
    q.addBindValue(userObj.userEmail);

    if (!q.exec())
    {
         qDebug() << q.lastError().text();
    }

    bool emailFound = false;

    while (q.next())
    {
        emailFound = true;
        qDebug() << "email exists";
        break;
    }


    if(emailFound){ // email exists:
        db.close();
        return false;
    }
    // add new user:

    q.prepare("INSERT INTO users (USERNAME, EMAIL, PASSWORD) VALUES (?,?,?)");
    q.addBindValue(userObj.userName);
    q.addBindValue(userObj.userEmail);
    q.addBindValue(userObj.userPassword);

    if (!q.exec())
    {
        qDebug() << q.lastError().text();
    }
    db.commit();
    db.close();

    return true;


}

bool OinkDB::login(OinkUser userObj){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/ghofranbabeir/Desktop/OinkProject/database.sqlite");
    if(!db.open()){
        qDebug() <<"cannot open database!";
    }

    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE USERNAME = ? AND PASSWORD = ?");
    q.addBindValue(userObj.userName);
    q.addBindValue(userObj.userPassword);

    if (!q.exec())
    {
          qDebug() << q.lastError().text();
    }

    bool exists = false;
    while (q.next())
    {

         exists = true;
    }
    db.close();

    return exists;
}

OinkUser OinkDB::findByEmail(QString email){
    OinkUser userObj;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/ghofranbabeir/Desktop/OinkProject/database.sqlite");
    if(!db.open()){
        qDebug() <<"cannot open database!";
    }

    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE EMAIL = ?");
    q.addBindValue(email);

    if (!q.exec())
    {
          qDebug() << q.lastError().text();
    }


    while (q.next())
    {
        userObj.userName = q.value("USERNAME").toString();
        userObj.userPassword = q.value("PASSWORD").toString();
        userObj.userEmail = email;


    }
    db.close();

    return userObj;
}

OinkUser OinkDB::findByName(QString name){
    OinkUser userObj;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/ghofranbabeir/Desktop/OinkProject/database.sqlite");
    if(!db.open()){
        qDebug() <<"cannot open database!";
    }
    
    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE USERNAME = ?");
    q.addBindValue(name);
    
    if (!q.exec())
    {
        qDebug() << q.lastError().text();
    }
    
    
    while (q.next())
    {
        userObj.userEmail = q.value("EMAIL").toString();
        userObj.userPassword = q.value("PASSWORD").toString();
        userObj.userName = name;
        
        
    }
    db.close();
    
    return userObj;
}

bool OinkDB::forgetPassword(QString email){
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/Users/ghofranbabeir/Desktop/OinkProject/database.sqlite");
    if(!db.open()){
        qDebug() <<"cannot open database!";
    }

    QSqlQuery q(db);
    q.prepare("SELECT * FROM users WHERE EMAIL = ?");
    q.addBindValue(email);

    if (!q.exec())
    {
          qDebug() << q.lastError().text();
    }

    QString pass ;
    bool found = false;
    while (q.next())
    {
        found  = true;
        pass = q.value("PASSWORD").toString();


    }
    db.close();
    if(!found)
        return false;
    Smtp* smtp = new Smtp("reset.password.oink@gmail.com", "reset123456", "smtp.gmail.com",465);
    connect(smtp, SIGNAL(status(QString)), this, SLOT(mailSent(QString)));

    smtp->sendMail("reset.password.oink@gmail.com", email , "Your Password","your password is "+pass);
    return true;
}
void OinkDB::mailSent(QString status)
{
    if(status == "Message sent")
        qDebug() <<"Message sent !";
}
