#ifndef OINKDB_H
#define OINKDB_H
#include <QObject>
#include <QDebug>
#include "oinkuser.h"
#include "smtp.h"

class OinkDB: public QObject
{
    Q_OBJECT

public slots:
    
    // mailSent function send the correct password to the actual user's email address.
    void mailSent(QString status);

public:
    explicit OinkDB(QObject *parent = 0);
    
    //registerUser function takes the user object (user name, password, email) and check whither the user name and email are not exisit then add the user object to the database and return true. Otherwise return false.
    bool registerUser(OinkUser userObj);
    
    // login function takes the user object (user name and password) and check whither bothe user name and password are exist the return true. Othewise return false.
    bool login(OinkUser userObj);
    
    //findByEmail function takes the email address and return the user object (name, password, email) whose email address match with given email.
    OinkUser findByEmail(QString email);
    
    //findByName function takes the name and return the user object (name, password, email) whose name match with given name.
    OinkUser findByName(QString name);
    
    //forgetPassword function taks the email address, fined and retreive the password and send the correct password to the actual email adress for the user.
    bool forgetPassword(QString email);



};

#endif // OINKDB_H
