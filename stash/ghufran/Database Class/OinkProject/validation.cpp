#include "validation.h"
#include <QRegExp>
Validation::Validation()
{
}

QString Validation::validData(QString name, QString mail, QString pass ,QString passConfirm){
    QString msg ="";
    if( !isUserName(name) ) {msg+="username;";}
    if( !isEmailAddress(mail) ) {msg+="email;";}
    if( !isPassword(pass) ){msg+="password;";}
    if( !matchedPassword(pass,passConfirm) ) {msg+="confirmPassword;";}
    return msg;
}

bool Validation::isUserName(QString strName)
{
    if ( strName.length() < 4 ) return false  ;
    return true;
}
bool Validation::isPassword(QString strPass)
{
    if ( strPass.length() < 5 ) return false  ;
    return true;
}
bool Validation::matchedPassword(QString strPass, QString strPass2)
{
    if (  strPass!= strPass2 ) return false  ;
    return true;
}
bool Validation::isEmailAddress(QString strEmailAddr)
{
    if ( strEmailAddr.length() == 0 ) return false  ;

    QRegExp rx("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    rx.setCaseSensitivity(Qt::CaseInsensitive);
    rx.setPatternSyntax(QRegExp::RegExp);

    return rx.exactMatch(strEmailAddr);
}
