#ifndef VALIDATION_H
#define VALIDATION_H
#include <QString>
class Validation
{
public:
    Validation();
    
    //isEmailAddress function takes the input email address and check whither the is in correct format, then eturn true if i is correct otherwise return false.
    bool isEmailAddress(QString strEmailAddr);
    
    //isUserName function takes the input user name and check whither the name is not less than four charachters then return true, otherwise return false.
    bool isUserName(QString strName);
    
    //isPassword function takes the input password and check whither the password is not less than five charachters then return true, otherwise return false.
    bool isPassword(QString strPass);
    
    //matchedPassword function takes the input password and input comfirmed password and check whither the two passwords are match then return true, otherwise return false.
    bool matchedPassword(QString strPass, QString strPass2);
    
    QString validData(QString name, QString mail, QString pass ,QString passConfirm);


};

#endif // VALIDATION_H
