#-------------------------------------------------
#
# Project created by QtCreator 2014-02-20T14:25:03
#
#-------------------------------------------------

QT       += core gui widgets network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Project
TEMPLATE = app


SOURCES += main.cpp\
        login.cpp

HEADERS  += login.h

FORMS    += login.ui

