#include "login.h"
#include "ui_login.h"
#include <QtSql>
#include <QRegExp>
#include <QMessageBox>



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentWidget(ui->login);

    // socket :
    isConnected = false;
    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    qDebug()<<"connecting..";
    socket->connectToHost("LocalHost",1234);
    if(!socket->waitForConnected(2000))
    {
        qDebug()<<"Error: "<<socket->errorString();
    }
    stopSignup= true;
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_loginButton_clicked()
{
    QString username = ui->username_lineEdit->text();
    QString password = ui->password_LineEdit->text();
    qDebug()<<"try to login";
    // connect to host
    if( isConnected )
    {
        QByteArray data;
        data.append(username+"\\"+password);

        qDebug()<<socket->write(data);


    }else{
        QMessageBox msgBox;
        msgBox.setText("server not found!");
        msgBox.exec();
    }


}


void MainWindow::on_signupButton_clicked() //login page sign up
{
    ui->username_label->setVisible(false);
    ui->email_label->setVisible(false);
    ui->pass_label->setVisible(false);
    ui->confirm_label->setVisible(false);
    ui->username_exist_label->setVisible(false);
    ui->email_exist_label->setVisible(false);

    ui->stackedWidget->setCurrentWidget(ui->signup);


}

void MainWindow::on_cancelButton_2_clicked() //form sign up
{

    ui->stackedWidget->setCurrentWidget(ui->login);
}



// This function gets called whenever the chat server has sent us some text:
void MainWindow::readyRead()
{   do{
    qDebug()<<"Ready";
    QString line = QString::fromUtf8(socket->readLine()).trimmed();
    //  Normal messges look like this: "username:The message"
    QRegExp messageRegex("^([^:]+):(.*)$");

    // Any message that starts with "/users:" is the server sending us a
    // list of users so we can show that list in our GUI:
    QRegExp usersRegex("^/users:(.*)$");
    // Is this a users message:
    if(usersRegex.indexIn(line) != -1)
    {
        // If so, udpate our users list on the right:
        QStringList users = usersRegex.cap(1).split(",");
        ui->userListWidget->clear();
        foreach(QString user, users)
             new QListWidgetItem(QPixmap(":/user.png"), user, ui->userListWidget);
        }
    // Is this a normal chat message:
    else if(messageRegex.indexIn(line) != -1)
    {
        // If so, append this message to our chat box:
        QString user = messageRegex.cap(1);
        QString message = messageRegex.cap(2);

        ui->roomTextEdit->append("<b>" + user + "</b>: " + message);
    }

    else if(line =="welcome"){
        ui->stackedWidget->setCurrentWidget(ui->page_3);
        // And send our username to the chat server.
        socket->write(QString("/me:" + ui->username_lineEdit->text() + "\n").toUtf8());
    }
    else if(line=="bye"){
        QMessageBox msgBox;
        msgBox.setText("invaild username or password");
        msgBox.exec();
    } else if (line=="emailexist"){
        stopSignup = true;
        ui->email_exist_label->setVisible(true);
    } else if (line=="userexist"){
        ui->username_exist_label->setVisible(true);
        stopSignup = true;
    }else if(line =="signedup"){
        qDebug()<<"sign up correctly";
        completeSignUp();
    }
    else if(line=="password-submitted"){
        QMessageBox msgBox;
        msgBox.setText("An email was sent to your email address.");
        msgBox.exec();
    }else if(line == "email not found"){
        QMessageBox msgBox;
        msgBox.setText("The email address you submitted was not found in the database.");
        msgBox.exec();
    }
    }while(socket->canReadLine());

}

// This function gets called when our socket has successfully connected  with correct username password comb.

void MainWindow::connected()
{
    qDebug()<<"connected";
    isConnected = true;

}

void MainWindow::disconnected()
{
    qDebug()<<"disconnected";
    isConnected = false;

}

void MainWindow::on_signupButton_2_clicked() //form sign up
{
    ui->username_label->setVisible(false);
    ui->username_exist_label->setVisible(false);
    ui->email_exist_label->setVisible(false);
    ui->email_label->setVisible(false);
    ui->pass_label->setVisible(false);
    ui->confirm_label->setVisible(false);
    QString name = ui->nameLineEdit_2->text();
    QString email = ui->EmailLineEdit_2->text();
    QString pass = ui->PasswordLineEdit_2->text();
    QString passConfirm = ui->confirmPasswordLineEdit_2->text();
    // Check with regex:
    QString msg=validData(name,email,pass,passConfirm);

    if(msg==""){//send data to server
        if( isConnected )
        {
            QByteArray data;
            data.append(name+"\\"+email+"\\"+pass);

            qDebug()<<socket->write(data);
            socket->waitForBytesWritten(30000);
            if(!stopSignup){

            }
        }else{
            QMessageBox msgBox;
            msgBox.setText("server not found!");
            msgBox.exec();
        }

    }
    else{
        // QMessageBox msgBox;
        // msgBox.setText(msg);
        // msgBox.exec();
    }

    // send to server:

}
void MainWindow::completeSignUp(){
    ui->stackedWidget->setCurrentWidget(ui->login);
    QMessageBox msgBox;
    msgBox.setText("user created successfully ! ");
    msgBox.exec();
}

QString MainWindow::validData(QString name, QString mail, QString pass ,QString passConfirm){
    QString msg ="";
    if( !isUserName(name) ) {msg+="wrong";ui->username_label->setVisible(true);};
    if( !isEmailAddress(mail) ) {msg+="wrong";ui->email_label->setVisible(true);};
    if( !isPassword(pass) ){msg+="wrong";ui->pass_label->setVisible(true);};
    if( !matchedPassword(pass,passConfirm) ) {msg+="wrong";ui->confirm_label->setVisible(true);};

    return msg;
}

bool MainWindow::isUserName(QString strName)
{
    if ( strName.length() < 4 ) return false  ;
    return true;
}
bool MainWindow::isPassword(QString strPass)
{
    if ( strPass.length() < 5 ) return false  ;
    return true;
}
bool MainWindow::matchedPassword(QString strPass, QString strPass2)
{
    if (  strPass!= strPass2 ) return false  ;
    return true;
}
bool MainWindow::isEmailAddress(QString strEmailAddr)
{
    if ( strEmailAddr.length() == 0 ) return false  ;

    QRegExp rx("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b");
    rx.setCaseSensitivity(Qt::CaseInsensitive);
    rx.setPatternSyntax(QRegExp::RegExp);

    return rx.exactMatch(strEmailAddr);
}

void MainWindow::on_forgetButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->forgetPage);

}

void MainWindow::on_resetButton_clicked()
{
    QString email = ui->resetEmailLineEdit_2->text();
    if( isConnected )
    {
        QByteArray data;
        data.append(email);

        qDebug()<<socket->write(data);
        ui->stackedWidget->setCurrentWidget(ui->login);

    }else{
        QMessageBox msgBox;
        msgBox.setText("server not found!");
        msgBox.exec();
    }
    ui->stackedWidget->setCurrentWidget(ui->login);
}

void MainWindow::on_sayButton_clicked()
{
    QString message = ui->sayLineEdit->text().trimmed();

    // Only send the text to the chat server if it's not empty:
    if(!message.isEmpty())
    {
        socket->write(QString(message + "\n").toUtf8());
    }

    // Clear out the input box so they can type something else:
    ui->sayLineEdit->clear();

    // Put the focus back into the input box so they can type again:
    ui->sayLineEdit->setFocus();
}
