#ifndef LOGIN_H
#define LOGIN_H

#include <QMainWindow>
#include <QTcpSocket>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void on_loginButton_clicked();

    void on_signupButton_clicked();


    void on_cancelButton_2_clicked();

    void on_signupButton_2_clicked();

    void on_forgetButton_clicked();

    void on_resetButton_clicked();

    void readyRead();

    void connected();

    void disconnected();


    void on_sayButton_clicked();

private:
    Ui::MainWindow *ui;
    QTcpSocket *socket;
    bool isEmailAddress(QString strEmailAddr);
    bool isUserName(QString strName);
    bool isPassword(QString strPass);
    bool matchedPassword(QString strPass, QString strPass2);
    QString validData(QString name, QString mail, QString pass ,QString passConfirm);
    bool isConnected;
    bool stopSignup;
    void completeSignUp();

};

#endif // LOGIN_H
