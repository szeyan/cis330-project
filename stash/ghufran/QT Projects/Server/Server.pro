#-------------------------------------------------
#
# Project created by QtCreator 2014-02-21T01:37:51
#
#-------------------------------------------------


QT       += core widgets network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT       -= gui

TARGET = Server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    myserver.cpp \
    smtp.cpp

HEADERS += \
    myserver.h \
    smtp.h

OTHER_FILES += \
    database.sqlite
