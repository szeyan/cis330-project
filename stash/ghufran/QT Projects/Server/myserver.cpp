﻿#include "myserver.h"
#include <QtSql>
#include <QSqlQuery>



MyServer::MyServer(QObject *parent):
    QObject(parent)
{
    server = new QTcpServer(this);
    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));

    if(!server->listen(QHostAddress::LocalHost,1234))
    {
        qDebug()<<"Server could not start!";
    }
    else
    {
        qDebug()<<"Server Started";
    }

}

void MyServer::newConnection()
{
    QTcpSocket *socket = server->nextPendingConnection();
    clients.insert(socket);

    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));


   // socket->write("hello client");
   // socket->flush();
}


void MyServer::readyRead()
{
    qDebug()<<"readRead";
    QTcpSocket *client = (QTcpSocket*)sender();

    do{
        QString line = QString::fromUtf8(client->readLine()).trimmed();
        qDebug() << "Read line:" << line;

        QRegExp meRegex("^/me:(.*)$");

        if(meRegex.indexIn(line) != -1)
        {
            QString user = meRegex.cap(1);
            users[client] = user;
            foreach(QTcpSocket *client, clients)
                client->write(QString("Server:" + user + " has joined.\n").toUtf8());
            sendUserList();
        }
        else if(users.contains(client))
        {
            QString message = line;
            QString user = users[client];
            qDebug() << "User:" << user;
            qDebug() << "Message:" << message;

            foreach(QTcpSocket *otherClient, clients)
                otherClient->write(QString(user + ":" + message + "\n").toUtf8());
        }
        else{

            // query[0] username  [1] email [2] pass in sign up page
             // query[0] username  [1] password in login page
            QStringList query = line.split("\\");
            if(query.size()==2) //login
            {
                QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
                db.setDatabaseName("/Users/ghofranbabeir/Desktop/QT Projects/Server/database.sqlite");
                if(!db.open()){
                    qDebug() <<"cannot open database!";
                }

                QSqlQuery q(db);
                q.prepare("SELECT * FROM users WHERE USERNAME = \""+query[0]+"\" AND PASSWORD = \""+query[1]+"\"");
                if (!q.exec())
                {
                    qDebug() << q.lastError().text();
                }
                bool exist = false;
                while (q.next())
                {
                    QVariant v = q.value("USERNAME");
                    qDebug()<< v.toString();
                    exist = true;
                }
                db.close();
                if(exist)
                    client->write("welcome");
                else
                    client->write("bye");
            }else if(query.size()==3) {
                QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
                db.setDatabaseName("/Users/ghofranbabeir/Desktop/QT Projects/Server/database.sqlite");
                if(!db.open()){
                    qDebug() <<"cannot open database!";
                }

                // check email exist :
                QSqlQuery q2(db);
                q2.prepare("SELECT * FROM users WHERE EMAIL = ?");
                q2.addBindValue(query[1]);

                if (!q2.exec())
                {
                    qDebug() << q2.lastError().text();
                }
                bool emailexist = false;

                while (q2.next())
                {
                    emailexist = true;
                    qDebug() << "email exist";
                    break;
                }
                if(emailexist){
                     client->write("emailexist");
                }else{
                    QSqlQuery q3(db);
                    q3.prepare("SELECT * FROM users WHERE USERNAME = ?");
                    q3.addBindValue(query[0]);

                    if (!q3.exec())
                    {
                        qDebug() << q3.lastError().text();
                    }
                    bool userexist = false;

                    while (q3.next())
                    {
                        userexist = true;
                        qDebug() << "username exist ";
                        break;
                    }
                    if(userexist){
                        client->write("userexist");
                    }else{
                        QSqlQuery q(db);
                        q.prepare("INSERT INTO users (USERNAME, EMAIL, PASSWORD) VALUES (?,?,?)");
                        q.addBindValue(query[0]);
                        q.addBindValue(query[1]);
                        q.addBindValue(query[2]);
                        if (!q.exec())
                        {
                            qDebug() << q.lastError().text();
                        }
                        client->write("signedup");
                        db.commit();
                    }
                }


                db.close();
            }else if(query.size()>-1) { // forget password
                QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
                db.setDatabaseName("/Users/ghofranbabeir/Desktop/QT Projects/Server/database.sqlite");
                if(!db.open()){
                    qDebug() <<"cannot open database!";
                }

                QSqlQuery q(db);
                q.prepare("SELECT * FROM users WHERE EMAIL = ?");
                q.addBindValue(query[0]);

                if (!q.exec())
                {
                    qDebug() << q.lastError().text();
                }
                bool exist = false;
                QString pass;
                QString emailTo;
                while (q.next())
                {
                    QVariant v = q.value("PASSWORD");

                    qDebug()<< v.toString();
                    pass = v.toString();
                    QVariant v2 = q.value("EMAIL");
                    emailTo = v2.toString();
                    exist = true;
                }

                db.close();
                if(exist)
                {/*
                    pass= "/password:"+pass;
                    client->write(pass.toUtf8());*/

                    // send  msg to user
                    Smtp* smtp = new Smtp("reset.pass.qt@gmail.com", "reset123456", "smtp.gmail.com",465);
                    connect(smtp, SIGNAL(status(QString)), this, SLOT(mailSent(QString)));

                    smtp->sendMail("reset.password.qt@gmail.com", emailTo , "Your Password in Oink chatting","your password is "+pass);
                    client->write("password-submitted");
                }
                else
                    client->write("email not found");
            }
        }
    } while(client->canReadLine());
}

void MyServer::disconnected()
{
    QTcpSocket *client = (QTcpSocket*)sender();
    qDebug() << "Client disconnected:" << client->peerAddress().toString();

    clients.remove(client);

    QString user = users[client];
    users.remove(client);

    sendUserList();
    foreach(QTcpSocket *client, clients)
        client->write(QString("Server:" + user + " has left.\n").toUtf8());
}

void MyServer::sendUserList()
{
    QStringList userList;
    foreach(QString user, users.values())
        userList << user;

    foreach(QTcpSocket *client, clients)
        client->write(QString("/users:" + userList.join(",") + "\n").toUtf8());
}

void MyServer::mailSent(QString status)
{
    if(status == "Message sent")
        qDebug() <<"Message sent !";
}
