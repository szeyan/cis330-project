#ifndef MYSERVER_H
#define MYSERVER_H

#include <QObject>
#include <QDebug>
#include <qtcpserver.h>
#include <qtcpsocket.h>
#include <QMap>
#include <QSet>
#include "smtp.h"

class MyServer : public QObject
{
    Q_OBJECT
public:
    explicit MyServer(QObject *parent = 0);
signals:

public slots:
    void newConnection ();
    void readyRead() ;
    void disconnected();
    void sendUserList();
    void mailSent(QString);


private:
    QTcpServer *server;
    QSet<QTcpSocket*> clients;
    QMap<QTcpSocket*,QString> users;


};

#endif // MYSERVER_H
