/********************************************************************************
** Form generated from reading UI file 'login.ui'
**
** Created by: Qt User Interface Compiler version 5.2.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGIN_H
#define UI_LOGIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QStackedWidget *stackedWidget;
    QWidget *login;
    QLineEdit *username_lineEdit;
    QPushButton *signupButton;
    QLineEdit *password_LineEdit;
    QPushButton *loginButton;
    QPushButton *forgetButton;
    QLabel *label_6;
    QLabel *label_7;
    QWidget *forgetPage;
    QPushButton *resetButton;
    QLineEdit *resetEmailLineEdit_2;
    QLabel *label_5;
    QWidget *signup;
    QPushButton *signupButton_2;
    QPushButton *cancelButton_2;
    QLineEdit *nameLineEdit_2;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *EmailLineEdit_2;
    QLabel *label_3;
    QLineEdit *PasswordLineEdit_2;
    QLabel *label_4;
    QLineEdit *confirmPasswordLineEdit_2;
    QLabel *username_label;
    QLabel *email_label;
    QLabel *pass_label;
    QLabel *confirm_label;
    QLabel *username_exist_label;
    QLabel *email_exist_label;
    QWidget *page_3;
    QLineEdit *sayLineEdit;
    QTextEdit *roomTextEdit;
    QPushButton *sayButton;
    QListWidget *userListWidget;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(778, 593);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        stackedWidget = new QStackedWidget(centralWidget);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        stackedWidget->setGeometry(QRect(10, -10, 771, 541));
        login = new QWidget();
        login->setObjectName(QStringLiteral("login"));
        username_lineEdit = new QLineEdit(login);
        username_lineEdit->setObjectName(QStringLiteral("username_lineEdit"));
        username_lineEdit->setGeometry(QRect(100, 60, 171, 20));
        signupButton = new QPushButton(login);
        signupButton->setObjectName(QStringLiteral("signupButton"));
        signupButton->setGeometry(QRect(190, 130, 75, 23));
        password_LineEdit = new QLineEdit(login);
        password_LineEdit->setObjectName(QStringLiteral("password_LineEdit"));
        password_LineEdit->setGeometry(QRect(100, 90, 171, 20));
        password_LineEdit->setEchoMode(QLineEdit::Password);
        loginButton = new QPushButton(login);
        loginButton->setObjectName(QStringLiteral("loginButton"));
        loginButton->setGeometry(QRect(110, 130, 75, 23));
        forgetButton = new QPushButton(login);
        forgetButton->setObjectName(QStringLiteral("forgetButton"));
        forgetButton->setGeometry(QRect(110, 170, 191, 23));
        label_6 = new QLabel(login);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(-20, 60, 111, 20));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_7 = new QLabel(login);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(-30, 90, 111, 21));
        label_7->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        stackedWidget->addWidget(login);
        forgetPage = new QWidget();
        forgetPage->setObjectName(QStringLiteral("forgetPage"));
        resetButton = new QPushButton(forgetPage);
        resetButton->setObjectName(QStringLiteral("resetButton"));
        resetButton->setGeometry(QRect(220, 120, 171, 23));
        resetEmailLineEdit_2 = new QLineEdit(forgetPage);
        resetEmailLineEdit_2->setObjectName(QStringLiteral("resetEmailLineEdit_2"));
        resetEmailLineEdit_2->setGeometry(QRect(190, 80, 221, 21));
        label_5 = new QLabel(forgetPage);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(20, 80, 171, 16));
        stackedWidget->addWidget(forgetPage);
        signup = new QWidget();
        signup->setObjectName(QStringLiteral("signup"));
        signupButton_2 = new QPushButton(signup);
        signupButton_2->setObjectName(QStringLiteral("signupButton_2"));
        signupButton_2->setGeometry(QRect(170, 190, 75, 23));
        cancelButton_2 = new QPushButton(signup);
        cancelButton_2->setObjectName(QStringLiteral("cancelButton_2"));
        cancelButton_2->setGeometry(QRect(250, 190, 75, 23));
        nameLineEdit_2 = new QLineEdit(signup);
        nameLineEdit_2->setObjectName(QStringLiteral("nameLineEdit_2"));
        nameLineEdit_2->setGeometry(QRect(130, 40, 231, 20));
        label = new QLabel(signup);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 40, 111, 20));
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_2 = new QLabel(signup);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 70, 111, 20));
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        EmailLineEdit_2 = new QLineEdit(signup);
        EmailLineEdit_2->setObjectName(QStringLiteral("EmailLineEdit_2"));
        EmailLineEdit_2->setGeometry(QRect(130, 70, 231, 20));
        label_3 = new QLabel(signup);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 100, 111, 20));
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        PasswordLineEdit_2 = new QLineEdit(signup);
        PasswordLineEdit_2->setObjectName(QStringLiteral("PasswordLineEdit_2"));
        PasswordLineEdit_2->setGeometry(QRect(130, 100, 231, 20));
        PasswordLineEdit_2->setEchoMode(QLineEdit::Password);
        label_4 = new QLabel(signup);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(0, 130, 121, 20));
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        confirmPasswordLineEdit_2 = new QLineEdit(signup);
        confirmPasswordLineEdit_2->setObjectName(QStringLiteral("confirmPasswordLineEdit_2"));
        confirmPasswordLineEdit_2->setGeometry(QRect(130, 130, 231, 20));
        confirmPasswordLineEdit_2->setEchoMode(QLineEdit::Password);
        username_label = new QLabel(signup);
        username_label->setObjectName(QStringLiteral("username_label"));
        username_label->setEnabled(true);
        username_label->setGeometry(QRect(370, 40, 271, 20));
        QFont font;
        font.setUnderline(false);
        username_label->setFont(font);
        username_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        email_label = new QLabel(signup);
        email_label->setObjectName(QStringLiteral("email_label"));
        email_label->setGeometry(QRect(370, 70, 281, 20));
        email_label->setFont(font);
        email_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        pass_label = new QLabel(signup);
        pass_label->setObjectName(QStringLiteral("pass_label"));
        pass_label->setGeometry(QRect(370, 100, 261, 20));
        pass_label->setFont(font);
        pass_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        confirm_label = new QLabel(signup);
        confirm_label->setObjectName(QStringLiteral("confirm_label"));
        confirm_label->setGeometry(QRect(370, 130, 371, 20));
        confirm_label->setFont(font);
        confirm_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        username_exist_label = new QLabel(signup);
        username_exist_label->setObjectName(QStringLiteral("username_exist_label"));
        username_exist_label->setEnabled(true);
        username_exist_label->setGeometry(QRect(370, 40, 261, 20));
        username_exist_label->setFont(font);
        username_exist_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        email_exist_label = new QLabel(signup);
        email_exist_label->setObjectName(QStringLiteral("email_exist_label"));
        email_exist_label->setEnabled(true);
        email_exist_label->setGeometry(QRect(370, 70, 271, 20));
        email_exist_label->setFont(font);
        email_exist_label->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        stackedWidget->addWidget(signup);
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        sayLineEdit = new QLineEdit(page_3);
        sayLineEdit->setObjectName(QStringLiteral("sayLineEdit"));
        sayLineEdit->setGeometry(QRect(2, 507, 708, 20));
        roomTextEdit = new QTextEdit(page_3);
        roomTextEdit->setObjectName(QStringLiteral("roomTextEdit"));
        roomTextEdit->setGeometry(QRect(2, 0, 431, 500));
        roomTextEdit->setReadOnly(true);
        sayButton = new QPushButton(page_3);
        sayButton->setObjectName(QStringLiteral("sayButton"));
        sayButton->setGeometry(QRect(716, 506, 50, 23));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(sayButton->sizePolicy().hasHeightForWidth());
        sayButton->setSizePolicy(sizePolicy);
        sayButton->setMaximumSize(QSize(50, 16777215));
        userListWidget = new QListWidget(page_3);
        userListWidget->setObjectName(QStringLiteral("userListWidget"));
        userListWidget->setGeometry(QRect(510, 0, 256, 500));
        stackedWidget->addWidget(page_3);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 778, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        username_lineEdit->setText(QString());
        signupButton->setText(QApplication::translate("MainWindow", "Sign up", 0));
        loginButton->setText(QApplication::translate("MainWindow", "Login", 0));
        forgetButton->setText(QApplication::translate("MainWindow", "Forget Your Password ?", 0));
        label_6->setText(QApplication::translate("MainWindow", "User Name", 0));
        label_7->setText(QApplication::translate("MainWindow", "Password", 0));
        resetButton->setText(QApplication::translate("MainWindow", "Reset My Password", 0));
        label_5->setText(QApplication::translate("MainWindow", "Enter Your Email Address:", 0));
        signupButton_2->setText(QApplication::translate("MainWindow", "Sign Up", 0));
        cancelButton_2->setText(QApplication::translate("MainWindow", "Cancel", 0));
        label->setText(QApplication::translate("MainWindow", "User Name", 0));
        label_2->setText(QApplication::translate("MainWindow", "Email Address", 0));
        label_3->setText(QApplication::translate("MainWindow", "Password", 0));
        PasswordLineEdit_2->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "Confirm Password", 0));
        username_label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">Username must be at least 4 characters.</span></p></body></html>", 0));
        email_label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">You did not submit a valid email address.</span></p></body></html>", 0));
        pass_label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">Password must be at least 5 characters.</span></p></body></html>", 0));
        confirm_label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">The password and password confirmation do not match.</span></p></body></html>", 0));
        username_exist_label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">Username already exists.</span></p></body></html>", 0));
        email_exist_label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" color:#ff0000;\">Email already exists.</span></p></body></html>", 0));
        sayButton->setText(QApplication::translate("MainWindow", "Say", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGIN_H
