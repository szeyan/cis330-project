Oink Chat: Software Architect
===========
Responsible member(s): Szeyan Li

This role is responsible for constructing the overall design of the program.  They determine how the pieces of the program will flow together.  They are also automatically entered as members of all other roles (but will play no special part unless specified).

To-Dos:
    high priority:
        DONE-research software that can be used to create project
        -construct flow charts of the program execution
        DONE-modularize the application design and figure its dependencies
        DONE-outline the interface each part that the program needs to implement

Oink Chat: Network Client-Server Builder
===========
Responsible member(s): Xi Zhang, Hanxiao Zhang

This role is responsible for creating the underlying chat functionality on which the GUI will run on.  They will use the database and the objects that are created to send and receive messages from the client(s) to the server and vice-versa.

To-Dos:
    high priority:
        -create a TCP client and server that can echo to each other (see definitions for what TCP means)
        -extend your implementation of a TCP client and server into a chatroom that can accept multiple client connections (let's say the max will be 100)
        -extend your implementation to be able to read in serialized objects (look in definitions)
        -extend your implementation to print out the username (inside a UserObj) of the client in the chatroom
        -extend your implementation to keep track of users connected to the server
        -extend your implementation to checking the if the connecting client is registered (this is a login)
        -extend your implementation to checking the if the connecting client wants to register
    medium priority:
        -extend your implementation to check for other things in the protocol
        -extend your implementation to allow for private chat
    low priority:
        -extend your implementation to allow sending of files or pictures over the network
        
Implementation:
    * will use the objects defined in the protocol
    * will call functions from the database
    * will use the specified cipher for encryption/decryption
    
Possible Resources:
    https://www.youtube.com/watch?v=A9W7cm94iAY
    https://www.youtube.com/watch?v=dw50MnqH-Qo ?
    https://www.youtube.com/watch?v=u5OdR46542M
    look at play list...on tcp, sockets, etc
    http://qt-project.org/doc/qt-4.8/network-network-chat.html
    http://qt-project.org/doc/qt-5.0/qtnetwork/examples-network.html
    serialization
        https://www.youtube.com/watch?v=JpmnHTM5qX8&list=PL2D1942A4688E9D63&index=84

Definitions:
    TCP - a connect-oriented network connection.  It ensures safe delivery of network packets from sender to receiver (no loss, no re-ordering.  Our messages will always arrive at the destination)
    
    UDP - a connectionless network connection.  It will send the message but will not ensure whether the message has arrived safely at its destination.  It basically just sends the message and then says "okay, not my responsibility no more."
    
    Serialization - basically, sometimes whether you're writing out to a file or sending a message over the network, sometimes you want to send an object you made.  Let's say a UserObj which contains members userName, password, and email.  Normally, you send characters or strings over the network or out to a file.  Serialization is the process of making it so that you can send/write your objects.  De-serialization is then the process of reading in back the object that was serialized.

Oink Chat: GUI Designer
===========
Responsible member(s): Szeyan Li

This role is responsible for designing the graphical user interface.  They are also responsible for linking anything other members made to the GUI.  It is a loose requirement that they also graphically create any icons that the chat application needs.

To-Dos:
    high priority:
        -logo
        -drawings of possible gui
        -create simple chatroom gui (with a who's on list)
        -create simple login gui
        -create simple register gui
    medium priority:
        -add an about popup
        -create private chat gui
        -make the gui look pretty (add in fonts, background, images)
        -allow for the reading in of a config file (to allow users to not have to always type their password)
        -add in sound features (oink, snort)
    low priority:
        -make the gui borderless and implement own window frame design
        -customize the program icon
        -allow users to select themes
        -allow users to select font/color
        -allow users to background
        -design emoticons 
        
Details:
    color scheme:
        -pink
        -white
        -dark purple
        -grey
    design style:
        -like AVG or Windows 8 like interface

Oink Chat: Objects Creator (Network Protocol Designer)
===========
Responsible member(s): Hanxiao Zhang

This role is responsible for creating the objects that are used by the Database and the underlying chat program.  This role is also responsible for defining how messages in the network should be written/read and interpreted.  They are responsible for ensuring that the other roles are using the protocol correctly.

To-Dos:
    high priority:
        -all classes listed in implementation
        
Implementation: 
    * all members will be private
    * all members will require getters and setters
    
    UserObj
        -string username
        -string password
        -string email
    MsgObj
        -string from (only values are in the following)
            -name of the server
            -name of the recipient user
        -string to (only values are in the following)
            -name of the server
            -name of the recipient user
        -string msg
    OinkObj
        -string ID (only values are in the following)
            -"server"
            -"client"
        -string Flag (only values are in the following)
            -"register"
            -"login"
            -"update"
            -"private"
            -"send"
            -"forgotAcc"
            -"closeConnection"
        -bool ACK
        -bool isCloseConnection
        -UserObj userobj
        -MsgObj msg
        -Pair<string user1, string user2> //this is for private chat
        -LinkedList/Array/Vector<string> updateList //this is a list of who's connected to the server
        
What each Flag would set:
    -"register"
        -UserObj will be set
            -username
            -email 
            -password 
        -ACK = true upon completion
    -"login"
        -UserObj will be set
            -username
            -password
        -ACK = true upon completion
    -"update"
        -LinkedList/Array/Vector<string> updateList will be set
            -to a string list of all logged on users
    -"private"
        -MsgObj will be set
            -from
            -to
            -msg
        -ACK = true upon completion
    -"send"
        -MsgObj will be set
            -from
            -to
            -msg
    -"forgotAcc"
        -UserObj will be set
            -email
        -ACK = true upon completion
    -"closeConnection"
        -isCloseConnection = true

Definitions:
    protocol - a set of rules of how something should be defined.  In this case, we will create a set of rules known as maybe Oink Protocol.  These rules will define how we should say something like "hey this is a login request" or "hey, this is a request from the client."  So, a protocol can define how messages should be written and how they should be interpreted when read in.    
    
    ACK - a fancy way of saying "acknowledged."  It's network jargon.  Most network entities like clients or servers will send an ACK saying "yes, I got your message" or "yes, I completed your request."  So, let's say a client wants to do a register request.  When the server receives this request, it will process it.  If all goes well, the server will send an ACK saying "yes, you're registered."  ACKs can be used in many other ways, but this is just a brief overview.

Oink Chat: Publisher & Documenter
===========
Responsible member(s):

This role is responsible making sure that the overall executable runs efficiently on the specified platform (Unix).  They are also responsible for reviewing the documentation (comments) that are written in each source file.  They are also responsible for writing the README.  

Finally, they are also responsible for compiling the presentation documents (PPT and demo video).  Don't worry.  You won't have to create the presentation documents yourself.  All other members will provide the things that they did to put into the presentation.  They will also be responsible for talking about the part they did in the project.

To-Dos:
    high priority:
        -try to deploy a simple gui (check resources below)
        -write a makefile that connects to QT libraries
    low priority:
        -convert our documentation to a nice looking word document or webpage or some online wiki (like assembla)

    each week:
        -update README
        -view current source files for good comments (picture if we put this in our resume!)
    last two weeks:
        -make sure makefile works for saucy/ix
        -make sure our source files works for saucy/ix
        -create demo video
        -create powerpoint for presentation

Implementation:
    README
        authors
        description 
        references
        relevant files
        notes/features
        code layout
        design decisions
        build/installation  
            -for unix
            -(for windows optional)
            -(for macs optional)
        usage
        
Possible Resources:
    https://www.youtube.com/watch?v=X1BsPQue5-k&list=PL2D1942A4688E9D63&index=85
    https://www.youtube.com/watch?v=chMNUzpN4pw&list=PL2D1942A4688E9D63&index=87
    http://askubuntu.com/questions/24270/dependency-walker-for-ubuntu
    http://www.wikihow.com/Create-Your-First-Qt-Program-on-Ubuntu-Linux
        
Oink Chat: Database Administrator
===========
Responsible member(s): Ghufran Babaeer

This role is responsible for writing a database that will keep track of users registered in the system.

To-Dos:
    high priority:
        -read in a text file 
        -write plain text username and password to the text file
        -extend your implementation to write each username and password pair (username,password) to a map
        -extend your implementation to read in serialized objects and put them into a map of <username, UserObj>
        -write serialized objects
    medium priority:
        -encrypt any strings inside the objects before writing them out to the map or file
        -decrypt any strings inside the objects before reading them from the map or file
    very low priority:
        -change implementation of text file database to a MySQL database
        
Interface (the minimum):
    * the server with some unique name will be the first entry into the database
    
    void init()
        -read in a text file or create one if it doesn't exist
        -store contents into map <username, UserObj>
    bool register(UserObj o)
        - check if username (inside UserObj) exists in map already (return false if it does)
        - check if email already exists (optional but good for findByEmail later on)
        - if it doesn't exist, add the new object to the map
        - open the database text file and write the userobject to the text file
    bool login(UserObj o)
        -check if o.username exists in our database
        -check if o.password matches the password stored in our database
    UserObj findByEmail(string email)
        -iterate through all keys in the map and determine if the email can be found in our map
        -return the UserObj if found or null if not
    UserObj findByName(string username)
        -use the find function to see if the username exists in our map
        -return if it does, null if not
        
Possible Resources:
    https://www.youtube.com/watch?v=4Mg6bw1MmAE&list=PL2D1942A4688E9D63&index=12
    ...the examples on the QT website
    serialization
        https://www.youtube.com/watch?v=JpmnHTM5qX8&list=PL2D1942A4688E9D63&index=84

Definitions:
    Serialization - basically, sometimes whether you're writing out to a file or sending a message over the network, sometimes you want to send an object you made.  Let's say a UserObj which contains members userName, password, and email.  Normally, you send characters or strings over the network or out to a file.  Serialization is the process of making it so that you can send/write your objects.  De-serialization is then the process of reading in back the object that was serialized.

Oink Chat: Security Manager
===========
Responsible member(s): all members

This role is responsible for encrypting and decrypting messages and/or objects that will be written to the database or sent over to the network.

To-Dos:
    high priority:
        -figure out which group member has the easiest/most efficient implementation of ASSN5
        -use the date cipher we wrote in class for any of our things that requires strings
    low priority:
        -use the QT libraries for encryption/decryption

Oink Chat: Tester
===========
Responsible member(s): all members

This role is responsible for testing the code that is written for the Oink Chat Application.

To-Dos:
    every single moment you're writing code:
        -check to make sure it compiles and can run
        -test it against bad inputs to make sure the program won't crash
    good to do but optional
        -test for memory leaks (check resources)
    very optional
        -use a fuzzer

Possible Resources:
    http://www.cprogramming.com/debugging/valgrind.html
    http://www.drmemory.org/
    https://code.google.com/p/bunny-the-fuzzer/
    http://peachfuzzer.com/
    
Definitions:
    Fuzzing - "Fuzz testing or fuzzing is a software testing technique, often automated or semi-automated, that involves providing invalid, unexpected, or random data to the inputs of a computer program. The program is then monitored for exceptions such as crashes, or failing built-in code assertions or for finding potential memory leaks. Fuzzing is commonly used to test for security problems in software or computer systems." - wiki