#include <QtCore/QCoreApplication>
#include <QtCore>
#include <QFile>
#include <QString>
#include <QMap>
#include <QDebug>

void save(QString path)
{
    int mynum = 2;;
    QMap<qint64, QString> map;
    map.insert(1, "one");
    map.insert(2, "two");
    map.insert(3, "three");

    //save it to disk
    QFile file(path+"/test.txt");

    if (!file.open(QIODevice::WriteOnly))
    {
        //if we can't open the file
        qDebug() << "Could not open file";
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_4_8);

    //write out
    out << mynum;
    out << map;

    file.flush();
    file.close();
}

void load(QString path)
{
    int mynum = 25;
    QMap<qint64, QString> map;

    //load it to disk
    QFile file(path+"/test.txt");

    if (!file.open(QIODevice::ReadOnly))
    {
        //if we can't open the file
        qDebug() << "Could not open file";
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_4_8);

    //read in
    in >> mynum;
    in >> map; //map gets overwritten by this read in map

    file.close();

    qDebug() << "mynum = " << mynum;

    foreach(QString item, map.values())
    {
        qDebug() << item;
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //--getting folders
    /*
    QString pwd("");
    char * PWD;
    PWD = getenv("PWD");
    pwd.append(PWD);*/

    //--getting current path of exe
    QString path = QCoreApplication::applicationDirPath();
    //qDebug() << "Working directory : " << path;

    //--save or load
    save(path);
    load(path);

    //--done
    qDebug() << "Done";
    return a.exec();
}
