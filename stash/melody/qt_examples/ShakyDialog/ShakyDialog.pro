#-------------------------------------------------
#
# Project created by QtCreator 2014-02-23T02:37:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ShakyDialog
TEMPLATE = app


SOURCES += main.cpp\
        shakydialog.cpp

HEADERS  += shakydialog.h

FORMS    += shakydialog.ui
