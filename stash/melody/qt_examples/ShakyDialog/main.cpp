#include "shakydialog.h"
#include <QApplication>
#include <QHBoxLayout>
#include <QPushButton>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    ShakyDialog dialog;
    QHBoxLayout layout(&dialog);
    QPushButton button("Push me.");
    layout.addWidget(&button);

    QObject::connect(&button, SIGNAL(clicked()), &dialog, SLOT(shake()));

    dialog.show();
    return app.exec();
}
