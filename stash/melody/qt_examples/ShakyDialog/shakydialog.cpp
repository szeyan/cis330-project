#include "shakydialog.h"
#include "ui_shakydialog.h"

ShakyDialog::ShakyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShakyDialog)
{
    ui->setupUi(this);
}

ShakyDialog::~ShakyDialog()
{
    delete ui;
}
