#ifndef SHAKYDIALOG_H
#define SHAKYDIALOG_H

#include <QDialog>
#include <QTimer>

//http://stackoverflow.com/questions/10677324/possible-to-make-qdialog-shake-in-osx
namespace Ui
{
class ShakyDialog;
}

class ShakyDialog : public QDialog
{
        Q_OBJECT

    public:
        explicit ShakyDialog(QWidget *parent = 0);
        ~ShakyDialog();

    public slots:
        void shake()
        {
            static int numTimesCalled = 0;
            numTimesCalled++;

            if (numTimesCalled == 9)
            {
                numTimesCalled = 0;
                return;
            }

            vacillate();
            QTimer::singleShot(40, this, SLOT(shake()));
        }

    private:
        Ui::ShakyDialog *ui;

        void vacillate()
        {
            QPoint offset(10, 0);

            move(((shakeSwitch) ? pos() + offset : pos() - offset));

            if(shakeSwitch){
                shakeSwitch = false;
            }else{
                shakeSwitch = true;
            }
        }

        bool shakeSwitch;
};

#endif // SHAKYDIALOG_H
