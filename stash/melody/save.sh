#!/bin/bash          
# ################################
# Bash Script
# Sze Yan Li
# ################################
# This script backs up files in OinkGUI
# ################################
#
## Location Variables
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#
## clear screen, build files
cd $DIR
name=OinkGUI
destDir=..
moveDir=../working_project
date=$(date +"%m_%d_%Y--%H_%M")
tarDest=$name--$date
#
## Tarball dir
cd $destDir
tar czf $tarDest.tar.gz ./$name
sleep 3
mv $tarDest.tar.gz $moveDir 
#
## DONE