#-------------------------------------------------
#
# Project created by QtCreator 2014-02-18T01:20:21
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OinkClient
TEMPLATE = app


SOURCES += main.cpp\
        oinkclient.cpp

HEADERS  += oinkclient.h

FORMS    += oinkclient.ui
