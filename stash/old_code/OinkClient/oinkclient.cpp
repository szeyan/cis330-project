#include "oinkclient.h"
#include "ui_oinkclient.h"

OinkClient::OinkClient(QWidget *parent) : QWidget(parent),
//    targetIpAddress(QHostAddress::LocalHost), targetPort(1234),
    targetIpAddress("10.111.220.27"), targetPort(1234), // Tony's ip address
    message(""), blockSize(0), ui(new Ui::OinkClient)
{
    ui->setupUi(this);

    // set up the socket
    tcpSocket = new QTcpSocket(this);
    tcpSocket->connectToHost(targetIpAddress,targetPort);

    connect(tcpSocket,SIGNAL(readyRead()),this,SLOT(readMessage()));
//    connect(tcpSocket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));


}

OinkClient::~OinkClient()
{
    delete ui;
}

// invoked when QIODevice() emits readyRead() signal
void OinkClient::readMessage() {
    {
        QDataStream in(tcpSocket);
        in.setVersion(QDataStream::Qt_5_2); // version needs to match OinkServer's.

        if(blockSize==0) { // if it's the first time client receives a message
            if(tcpSocket->bytesAvailable() < (int)sizeof(quint16)) return;  // 2 bytes is the size of an empty block; so it shouldn't be less than 2 bytes
            in >> blockSize; // enter the block size
        }
        if(tcpSocket->bytesAvailable() < blockSize) return; // the acutal message shouldn't be smaller than stated
        in >> message; // enter the message

        ui->interactDisplay->append(message);
        blockSize = 0;
    }
}

void OinkClient::on_sendButton_clicked(){

    if (!ui->lineEdit->text().isEmpty()) {
        tcpSocket->write(ui->lineEdit->text().toUtf8()); // write to somewhere. the second line of OinkServer.broadCastClient gets the message
        tcpSocket->flush();
        ui->lineEdit->clear();
//        emit tcpSocket->readyRead();
    }
}
