#ifndef OINKCLIENT_H
#define OINKCLIENT_H

#include <QWidget>
#include <QTcpSocket>
#include <QTcpServer>

namespace Ui {
class OinkClient;
}

class OinkClient : public QWidget
{
    Q_OBJECT

public:
    explicit OinkClient(QWidget *parent = 0);
    ~OinkClient();

public slots:
    void readMessage(); // connected to QTcpSocket readyRead() signal

private slots:
    void on_sendButton_clicked();

private:
    Ui::OinkClient *ui;
    QTcpSocket *tcpSocket;
    QTcpServer *tcpServer;
    QString targetIpAddress;
    int targetPort;

    QString message; // from the server
    quint16 blockSize;
};

#endif // OINKCLIENT_H
