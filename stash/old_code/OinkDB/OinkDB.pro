#-------------------------------------------------
#
# Project created by QtCreator 2014-03-02T22:56:33
#
#-------------------------------------------------

QT       += core gui sql network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OinkDB
TEMPLATE = app

SOURCES += main.cpp\
            oinkdb.cpp \

HEADERS  += oinkdb.hpp \
