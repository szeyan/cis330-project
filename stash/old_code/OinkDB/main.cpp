#include <QDebug>
#include "oinkdb.hpp"

int main(int argc, char *argv[])
{
    //--INIT DATABASE
    QString dirToDatabaseFile = "C:/Users/Melody/Dropbox/CIS330/cis330-project/OinkDB";
    OinkDB db(dirToDatabaseFile);

    //note: be sure to use the sqlite database browser to view the records thus far

    //--TEST REGISTRATION
    //only valid entry == melody, 123456, melodyli.sy@gmail.com
    {
        db.registerUser("melody", "123456", "melodyli.sy@gmail.com");

        bool r2 = db.registerUser("Melody", "123456", "melodyli.ss@gmail.com");
        qDebug() << "register - case sensitive username - (should be false): " << r2;

        r2 = db.registerUser("bob", "123456", "melodyli.SY@gmail.com");
        qDebug() << "register - case sensitive email - (should be false): " << r2;
    }

    //--TEST LOGIN
    {
        qDebug() << "login - 'melody': " << db.loginUser("melody", "123456");
        bool r2 = db.loginUser("Melody", "123456");
        qDebug() << "login - 'Melody' (should be false): " << r2;
    }

    //--TEST getUsername
    {
        QString username = db.getUsername("melodyli.sy@gmail.com");
        qDebug() << "getUsername - 'melodyli.sy@gmail.com': " << username;
        username = db.getUsername("Melodyli.sy@gmail.com");
        qDebug() << "getUsername - 'Melodyli.sy@gmail.com': " << username;
        username = db.getUsername("m3lodyli.sy@gmail.com");
        qDebug() << "getUsername - 'm3lodyli.sy@gmail.com' (should be empty): " << username;
    }

    //--TEST getPassword
    {
        QString password = db.getPassword("melody");
        qDebug() << "getPassword - 'melody': " << password;
        password = db.getPassword("Melody");
        qDebug() << "getPassword - 'Melody' (should be empty): " << password;
        password = db.getPassword("bob");
        qDebug() << "getPassword - 'bob' (should be empty): " << password;
    }

    return 0;
}
