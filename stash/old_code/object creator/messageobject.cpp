#include "messageobject.hpp"

MessageObject::MessageObject() : message(""), sendingServer(""), sendingUser(""), receivingServer(""), receivingUser("") {

}
 
MessageObject::~MessageObject() {

}

string MessageObject::getMessage() const {
	return this->message;
}

string MessageObject::getSendingServer() const {
	return this->sendingServer;
}

string MessageObject::getSendingUser() const {
	return this->sendingUser;
}

string MessageObject::getReceivingServer() const {
	return this->receivingServer;
}

string MessageObject::getReceivingUser() const {
	return this->receivingUser;
}
		
void MessageObject::setMessage(string message) {
	this->message = message;
}

void MessageObject::setSendingServer(string sendingServer) {
	this->sendingServer = sendingServer;
}

void MessageObject::setSendingUser(string sendingUser) {
	this->sendingUser = sendingUser;
}

void MessageObject::setReceivingServer (string receivingServer) {
	this->receivingServer = receivingServer;
}

void MessageObject::setReceivingUser(string receivingUser) {
	this->receivingUser = receivingUser;
}
