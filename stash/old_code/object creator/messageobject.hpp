#ifndef MESSAGEOBJECT_H
#define MESSAGEOBJECT_H

#include <string>
using namespace std;

class MessageObject {
	
	public:
		MessageObject();
		~MessageObject();
		string getMessage() const;
		string getSendingServer() const;
		string getSendingUser() const;
		string getReceivingServer() const;
		string getReceivingUser() const;
		void setMessage(string);
		void setSendingServer(string);
		void setSendingUser(string);
		void setReceivingServer(string);
		void setReceivingUser(string);





	private:
		string message;
		string sendingServer;
		string sendingUser;
		string receivingServer;
		string receivingUser;


} ;

#endif
