#include "UserObj.hpp"
#include "messageobject.hpp"
#include "oinkobject.hpp"

OinkObject::OinkObject() {

}

OinkObject::~OinkObject() {

}

bool OinkObject::isAck() const {
		return ACK;
	}

void OinkObject::setAck(bool ack) {
	ACK = ack;
}

bool OinkObject::isIsCloseConnection() const {
	return isCloseConnection;
}

void OinkObject::setIsCloseConnection(bool isCloseConnection) {
	this->isCloseConnection = isCloseConnection;
}

const MessageObject& OinkObject::getMessage() const {
	return message;
}

void OinkObject::setMessage(const MessageObject& message) {
	this->message = message;
}

UserObj OinkObject::getUser() const {
	return user;
}

void OinkObject::setUser(UserObj user) {
	this->user = user;
}

const vector<string>& OinkObject::getUserList() const {
	return userList;
}

void OinkObject::setUserList(const vector<string>& userList) {
	this->userList = userList;
}
