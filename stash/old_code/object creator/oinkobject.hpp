#ifndef OINKOBJECT_H
#define OINKOBJECT_H
#include <vector>
#include <string>
#include "messageobject.hpp"
#include "userobj.hpp"


class OinkObject {

public:
	OinkObject();
	~OinkObject();

	bool isAck() const;
	void setAck(bool);
	bool isIsCloseConnection() const;
	void setIsCloseConnection(bool);
	const MessageObject& getMessage() const;
	void setMessage(const MessageObject&);
	UserObj getUser() const;
	void setUser(UserObj);
	const vector<string>& getUserList() const;
	void setUserList(const vector<string>&);

	enum ID {server, client};
	enum Flag {registeration, login, update, privatechat, send, forgotAcc, closeConnection};


private:
	bool ACK;
	bool isCloseConnection;
	UserObj user;
	MessageObject message;
	vector<string> userList;

};



#endif
