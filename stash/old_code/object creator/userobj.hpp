#ifndef USEROBJ_HPP
#define USEROBJ_HPP

#include <string>

class UserObj        
{
private:
    std::string name;
    std::string password;
    std::string email;
public:
	// constructor and destructor
	UserObj();
	~UserObj();
	
	//set and get name
    void setName(std::string _name);
    std::string getName() const;

    //set and get password
    void setPassword(std::string _password); 
    std::string getPassword() const;
    
    //set and get email
    void setEmail(std::string _email); 
    std::string getEmail() const;
};


#endif //USEROBJ_HPP