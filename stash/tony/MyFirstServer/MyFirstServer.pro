#-------------------------------------------------
#
# Project created by QtCreator 2014-02-12T01:23:30
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = MyFirstServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    myserver.cpp

HEADERS += \
    myserver.h
