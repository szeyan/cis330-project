#include "myserver.h"
#include <QtCore/QCoreApplication>
#include <QtCore>
#include <QFile>
#include <QString>
#include <QMap>
#include <QDebug>

MyServer::MyServer(QObject *parent) :
    QObject(parent)
{
    server = new QTcpServer();


    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection())); // a client connects to a server

    if (!server->listen(QHostAddress("10.111.79.141"),1234)) { //listen to a client that is connected to it
        qDebug() << "Server does not start" ;

    } else {
        qDebug() << "Server started" ;
    }
    qDebug() << server->serverAddress();
}


void MyServer::newConnection(){
    QTcpSocket *socket = server->nextPendingConnection(); // ask server for a nest pending connection
    qDebug() << "Client connection" ;
    socket->write("hello client \r\n");
    socket->flush();

    socket->waitForBytesWritten(3000);

    socket->close();
}
