#-------------------------------------------------
#
# Project created by QtCreator 2014-02-17T19:40:59
#
#-------------------------------------------------

QT       += core
QT       += network
QT       -= gui

TARGET = OinkServer
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    oinkserver.cpp \
    ../OintClient/oinkclient.cpp

HEADERS += \
    oinkserver.h
