#include "oinkserver.h"

OinkServer::OinkServer(QObject *parent) : QObject(parent){
    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(getNewClient()));

    if (tcpServer->listen(QHostAddress::LocalHost,1234)) { //listen to a client that is connected to it. repla
        qDebug() << "Server started" ;
    } else {
        qDebug() << "Server did not start" ;
    }

}

void OinkServer::getNewClient() {
    QTcpSocket* tcpSocket = tcpServer->nextPendingConnection();
    users.push_back(tcpSocket);

    connect(tcpSocket,SIGNAL(disconnected()),tcpSocket,SLOT(loseOneClient()));
    connect(tcpSocket,SIGNAL(disconnected()),tcpSocket,SLOT(deleteLater()));

    for (int i = 0; i < users.size(); i++) {
        users[i]->write("new user entered."); // need to modify so that a user's name shows instead
        users[i]->flush();
//      socket->waitForBytesWritten(10000);
    }



}

void OinkServer::loseOneClient() {

    for (int i = 0; i < users.size(); i++) {
        users[i]->write("new user entered."); // need to modify so that a user's name shows instead
        users[i]->flush();
//        socket->waitForBytesWritten(10000);
    }

}
