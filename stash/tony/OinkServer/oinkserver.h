#ifndef OINKSERVER_H
#define OINKSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QVector>

class OinkServer : public QObject
{
    Q_OBJECT
public:
    explicit OinkServer(QObject *parent = 0);

signals:

public slots:
    void getNewClient();
    void loseOneClient();

private:
    QTcpServer* tcpServer;
    QVector<QTcpSocket*> users;

};

#endif // OINKSERVER_H
