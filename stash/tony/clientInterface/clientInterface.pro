#-------------------------------------------------
#
# Project created by QtCreator 2014-02-09T23:39:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = clientInterface
TEMPLATE = app


SOURCES += main.cpp\
        login.cpp

HEADERS  += login.h

FORMS    += login.ui

RESOURCES += \
    resource/TonyResource.qrc
