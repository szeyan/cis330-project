#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QPixmap>
#include <QGraphicsPixmapItem>
#include <QtCore>
#include <QtGui>
#include "login.h"
#include "ui_login.h"

LogIn::LogIn(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LogIn)
{
    ui->setupUi(this);


    map = new QPixmap(":/pig_fin.png");
    item = new QGraphicsPixmapItem(*map);
    scene = new QGraphicsScene(this);
    scene->addItem(item);
    ui->graphics_view_piglets->ensureVisible ( scene->sceneRect()  );
    ui->graphics_view_piglets->fitInView(scene->sceneRect() ,Qt::KeepAspectRatio);
    ui->graphics_view_piglets->setScene(scene);
//    ui->graphics_view_piglets->scale(408/ui->graphics_view_piglets->size().width(),258/ui->graphics_view_piglets->size().height());
//    ui->graphics_view_piglets->fitInView(scene->itemsBoundingRect() ,Qt::KeepAspectRatio);

}

LogIn::~LogIn()
{
    delete ui;
    delete scene;
    delete item;
    delete map;
}

void LogIn::on_push_button_login_clicked()
{
    ui->line_edit_username->setText("No Username???");
}
