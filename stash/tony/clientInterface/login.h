#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QPixmap>
#include <QtCore>
#include <QtGui>


namespace Ui {
class LogIn;
}

class LogIn : public QWidget
{
    Q_OBJECT

public:
    explicit LogIn(QWidget *parent = 0);
    ~LogIn();

private slots:
    void on_push_button_login_clicked();

private:
    Ui::LogIn *ui;
    QGraphicsScene *scene; // needs to use a pointer
    QGraphicsPixmapItem *item;
    QPixmap* map;

};

#endif // LOGIN_H
