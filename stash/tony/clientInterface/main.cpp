#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QtGui>
#include <QtCore>
#include "login.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

//  To display an image:
//    QGraphicsScene scene;
//    QGraphicsView view(&scene);
//    QGraphicsPixmapItem item(QPixmap(":/pig_fin.png"));
//    scene.addItem(&item);
//    view.show();


    LogIn w;
    w.show();

    return app.exec();
}
