#-------------------------------------------------
#
# Project created by QtCreator 2014-02-22T10:02:52
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = BinaryIO
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    userobj.cpp \

HEADERS += \
    userobj.hpp
