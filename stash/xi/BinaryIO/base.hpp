#ifndef BASE_HPP
#define BASE_HPP

#include <QObject>

class Base : public QObject
{
    Q_OBJECT
public:
    explicit Base(QObject *parent = 0);

signals:

public slots:

};

QDataStream &operator<<(QDataStream &ds, const Base &obj);
QDataStream &operator>>(QDataStream &ds, Base &obj) ;

#endif // BASE_HPP
