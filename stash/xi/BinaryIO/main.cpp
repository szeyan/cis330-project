#include <QCoreApplication>
#include <QtCore>
#include <QFile>
#include <QString>
#include <QMap>
#include <QDebug>
#include <QDataStream>

#include "userobj.hpp"

// save info to an file use serialization
void Save(){
    // make a map, with key -> userName, value -> userName, userEmail, userPassword
    QMap<QString, UserObj>map;

    UserObj ubj;
    // input info
    ubj.setName("welovecs");
    ubj.setEmail("welovecs@uoregon.edu");
    ubj.setPassword("welovecsuoregon");
    QString mapKey;
    mapKey = ubj.getName();

    // insert userobj into map
    map.insert(mapKey, ubj);

    //save to disk
    // Xi's laptop path
    QFile file("/Users/chenmeng/desktop/330/projectPractice/myfile3.txt");
    if(!file.open(QIODevice::WriteOnly))
    {
        qDebug() << "Could not open file";
        return;
    }

    //write into file
    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_5_2);

    out << map;

    file.flush();
    file.close();
}

void Load(){
    QMap<QString, UserObj>map;

    //load from disk
    // Xi's laptop path
    QFile file("/Users/chenmeng/desktop/330/projectPractice/myfile3.txt");
    if(!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "Could not open file";
        return;
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_5_2);

    in >> map;
//    map[""]; this could call for the sepcific key value, write between ""

    file.close();

    //print out the values
    foreach (UserObj item, map.values()) {
        qDebug () << item.getName() << item.getEmail() << item.getPassword();
    }
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Save();
    Load();
    return a.exec();
}
