#include <iostream>
#include <QString>
#include <QDataStream>

#include "userobj.hpp"

UserObj::UserObj():name(""),email(""),password(""){}

UserObj::UserObj(QString Name, QString Email, QString Password){
    name = Name;
    email = Email;
    password = Password;
}

UserObj::~UserObj(){}

void UserObj::setName(QString _name){
    this->name = _name;
}

QString UserObj::getName() const{
    return this->name;
}

void UserObj::setPassword(QString _password){
    this->password = _password;
}

QString UserObj::getPassword() const{
    return this->password;
}

void UserObj::setEmail(QString _email){
    this->email = _email;
}

QString UserObj::getEmail() const{
    return this->email;
}

// new operator defind <<
QDataStream &operator<<(QDataStream &out, const UserObj &ubj)
{
    out << ubj.getName() << ubj.getPassword()
        << ubj.getEmail();
    return out;
}

// new operator defind >>
QDataStream &operator>>(QDataStream &in, UserObj &ubj)
{
    QString theEmail;
    QString theName;
    QString thePassword;
    in >> theEmail >> theName >> thePassword;
    ubj = UserObj(theName, theEmail, thePassword);
    return in;
}
