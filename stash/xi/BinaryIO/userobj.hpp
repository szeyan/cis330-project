#ifndef USEROBJ_HPP
#define USEROBJ_HPP

#include <QMetaProperty>
#include <string>

class UserObj
{

private:
    QString name;
    QString password;
    QString email;
public:
    // constructor and destructor
    UserObj();
    // new constructor added
    UserObj(QString Name, QString Email, QString Password);
    ~UserObj();

    //set and get name
    void setName(QString _name);
    QString getName() const;

    //set and get password
    void setPassword(QString _password);
    QString getPassword() const;

    //set and get email
    void setEmail(QString _email);
    QString getEmail() const;
};

// redefine operators >> and <<
QDataStream &operator<<(QDataStream &out, const UserObj &ubj);
QDataStream &operator>>(QDataStream &in, UserObj &ubj);

#endif //USEROBJ_HPP
