			1. AUTHORS: Ghufran Babaeer, Xingwei Cao, Szeyan Li, Hanxiao Zhang, Xi Zhang
				Team Leader:  Szeyan Li
			
			2. DESCRIPTION what our project is about
			
			3. REFERENCES the tools we're using (describe what these tools are, provide a weblink link to them)
				- Doxygen  (http://www.stack.nl/~dimitri/doxygen/)
				- QtCreator and QtLibraries (http://qt-project.org/doc/qtcreator-2.8/)
				- MySQL (http://www.mysql.com)
				- Hanxiao's DateCipher from our class homework
				- NcFramelessHelper (http://qt-apps.org/content/show.php/Frameless+Window+Helper?content=138161)
				- Raivis Strogonovs's Qt SMTP Protocol (ask Ghufran about this part!)
				- GoAnimate (http://goanimate.com)
				- Prezi (http://prezi.com)
				- (Name of tool used to create demo video)
			
			4. CODE LAYOUT/RELEVANT FILES - basically the location of the index.html our Doxygen thing will have (do this part later)
			
			5. FEATURES the features we provide in our OinkChat users
				- registration (an email is also sent upon registration)
				- login
				- ability to retrieve your lost account (an email is also sent upon retrieval of user account information)
				- a config file to "remember" your last login details
				- chat room 
				- private chatting
				- ability to change fonts, sizes, backgrounds, sound on/off, window shakes for notifications
			
			6. DESIGN DECISIONS what we did
				- used Doxygen to construct our documentation
				- used QT to create our project
				- used SQL to keep a database of our registered users
				- used Hanxiao's DateCipher for encryption and decryption of all strings
				- create our own Oink Protocol for sending and receiving messages between client and server
				- coded validation into registration to make sure we have valid registered users
				- integrated STMP protocol, so that we could send users actual emails during registration or forgot username/password
				- made an enhanced GUI that doesn't use the operating system's default window frames
				- integrated ability to save/load a config file ("remember me" function)
			
			7. BUILD/INSTALLATION
				- give location of makefile
				- how to run makefile 
			
			8. USAGE
				- give location to our demo video  
				- start making powerpoint presentation 
				- include stuff you wrote in the readme 
				- ask SzeYan if you need design ideas 
				- make sure powerpoint isn't too cluttered but is long enough for a 5 minute introduction of our project