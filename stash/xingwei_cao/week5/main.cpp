#include <qapplication.h>
#include <qlabel.h>

int main( int argc, char *argv[] )
{
  QApplication myapp( argc, argv );

  QLabel *mylabel = new QLabel( "Hello World!!", 0 );
  mylabel->resize( 80, 30 );

  // myapp.setMainWidget( mylabel ); // 閉じるボタンを押したときアプリ終了(Qt3)
  mylabel->show();

  return myapp.exec();
}
