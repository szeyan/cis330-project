#include <iostream>
#include <string>

#include "userobj.hpp"

UserObj::UserObj():name(""),email(""),password(""){}

UserObj::~UserObj(){}

void UserObj::setName(std::string _name)
{
    this->name = _name;
}

std::string UserObj::getName() const 
{
	return this->name;
}

void UserObj::setPassword(std::string _password)
{
	this->password = _password;
}

std::string UserObj::getPassword() const 
{
	return this->password;
}

void UserObj::setEmail(std::string _email)
{
    this->email = _email;
}

std::string UserObj::getEmail() const 
{
	return this->email;
}
